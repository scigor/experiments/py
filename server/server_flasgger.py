#!/usr/bin/env python3

from flask import Flask, request, jsonify
from flasgger import Swagger, LazyString, LazyJSONEncoder
from flasgger import swag_from

import socket

app = Flask(__name__)

app.json_encoder = LazyJSONEncoder

swagger_template = dict(
    info = {    
        "swagger_info": "2.0",
        'title': 'My first Swagger UI document',
        #'title': LazyString(lambda: 'My first Swagger UI document'),
        'version': '0.1',
        #'version': LazyString(lambda: '0.1'),
        'description': 'This document depicts a Swagger UI document and implements Hello World functionality after executing GET.',
        #'description': LazyString(lambda: 'This document depicts a Swagger UI document and implements Hello World functionality after executing GET.'),
    },
    host = socket.gethostname()
    #host = LazyString(lambda: request.host)
)

swagger_config = {
    "headers": [],    
    "specs": [
        {        
        "version": "0.0.1",
        "title": "Api v1",
        "description": "This is the version 1 of our API",
        "endpoint": 'v1_spec',
        "route": '/v1/spec',
        "rule_filter": lambda rule: rule.endpoint.startswith(
            "should_be_v1_only"),
        # "model_filter": lambda definition: (
        #    "v1_model" in definition.tags)
        #"rule_filter": lambda rule: True,
        "model_filter": lambda tag: True,
        },
        {        
        "version": "0.0.2",
        "title": "Api v2",
        "description": "This is the version 2 of our API",
        "endpoint": 'v2_spec',
        "route": '/v2/spec',
        "rule_filter": lambda rule: rule.endpoint.startswith(
            "should_be_v2_only"),
        #"model_filter": lambda definition: (
        #    "v2_model" in definition.tags)
        #"rule_filter": lambda rule: True,
        "model_filter": lambda tag: True,
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/api/docs/"
}

swagger = Swagger (app, template=swagger_template, config=swagger_config )
#swagger = Swagger (app, config=swagger_config )

@swag_from("hello_world.yml", methods=['GET'])
@app.route("/v1/hello",endpoint='should_be_v1_only_hello')
def hello_world():
    return jsonify({"response":"Hello World!!!", "responseCode": "2","responseDesc": "SUCCESS"})

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=9090)  

