#!/usr/bin/env python3

from flask import Flask, request, jsonify, render_template, send_from_directory

from swagger_ui import flask_api_doc
#  Create a Flask WSGI application
app = Flask(__name__, template_folder="templates")  

#flask_api_doc(app, config_path='/static/hello_api.yml', url_prefix='/api/docs', title='API doc')

#  Create a URL route to this resource
class HelloWorld():
  @app.route('/')
  def get_root():
    print('sending root')
    return render_template('index.html')

  @app.route('/api/docs')
  def get_docs():
    print('sending docs')
    return render_template('swaggerui.html')

  @app.route('/spec')
  def get_spec():
    return send_from_directory(app.root_path, '/static/hello_api.yml')
     
  @app.route('/api')
  def get_api():
    hello_dict = {'en': 'Hello', 'es': 'Hola'}
    lang = request.args.get('lang')
    return jsonify(hello_dict[lang])

#  Start a development server
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, use_reloader=True, debug=True) 
