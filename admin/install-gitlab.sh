#!/bin/bash
#
#LEVEL: 3_0
#DESC:

sudo apt install -qq -y vim git curl >/dev/null 2>&1 
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash

export LC_TYPE=en.US.UTF-8
export LC_ALL=en.US.UTF-8

sudo apt install -y gitlab-ce

sed -i s/"gitlab.example.com"/"$(hostname).zteam.fr"/g /etc/gitlab/gitlab.rb
grep -i Password:  /etc/gitlab/initial_root_password

# verifier la config ipv6
grep -v '#' /etc/gitlab/gitlab.rb | egrep 'nginx|registry|puma'

#nginx['listen_addresses'] = ['*', '[::]']

#gitlab_rails['gitlab_default_projects_features_container_registry'] = true
#gitlab_rails['registry_enabled'] = true
#registry_external_url 'https://registry.zteam.fr'
#registry_nginx['enable'] = true
#registry_nginx['listen_addresses'] = ['[::]']
#registry_nginx['ssl_certificate'] = "/etc/gitlab/registry.zteam.fr.cert"
#registry_nginx['ssl_certificate_key'] = "/etc/gitlab/registry.zteam.fr.key"

# configuration apres gitlab-ctl
# /var/opt/gitlab/nginx/conf/

# log 
# /var/log/gitlab/nginx/

# cert registry
# sudo openssl req   -newkey rsa:4096 -nodes -sha256 -keyout registry.zteam.fr.key -addext "subjectAltName = DNS:registry.zteam.fr" -x509 -days 365 -out registry.zteam.fr.cert
