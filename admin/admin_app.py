#!/usr/bin/python3
from flask import Flask, jsonify
import subprocess

app = Flask(__name__)

@app.route('/run/<script_name>', methods=['GET'])
def run_script(script_name):
    result = subprocess.run([f"./{script_name}"], capture_output=True, text=True)
    return jsonify({
        "script": script_name,
        "output": result.stdout,
        "error": result.stderr,
        "return_code": result.returncode
    })


if __name__ == '__main__':
    app.run(debug=True)

