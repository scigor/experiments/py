#!/bin/bash
#
#LEVEL: 4
#DESC:

sudo apt-get install -y  apt-transport-https ca-certificates curl gnupg-agent
sudo apt-get install -y  software-properties-common

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get install -y docker-ce

