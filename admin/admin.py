#!/usr/bin/python3
import os
import signal
import sys
from pathlib import Path

HEADER_FILE = "HEADER"  # Nom du fichier contenant l'ASCII header
LEVEL_SEPARATOR = "_"


def load_header():
    os.system(f"clear")
    """Charge et affiche le contenu du fichier HEADER"""
    if os.path.exists(HEADER_FILE):
        with open(HEADER_FILE, 'r') as header_file:
            print(header_file.read())


def parse_scripts(level_prefix, is_root):
    """Analyse les fichiers du répertoire courant et retourne ceux correspondant au niveau"""
    scripts = []
    for file_name in os.listdir('.'):
        if not (file_name.endswith('.sh') or file_name.endswith('.py')):
            continue

        with open(file_name, 'r') as file:
            lines = file.readlines()

        level_line = next((line for line in lines if line.startswith('#LEVEL:')), None)
        desc_line = next((line for line in lines if line.startswith('#DESC:')), None)

        if level_line:
            script_level = level_line.split(":")[1].strip()
            description = desc_line.split(":")[1].strip() if desc_line else ""

            # Filtrer les scripts en fonction du niveau
            cond = script_level.startswith(level_prefix) and len(level_prefix.split(LEVEL_SEPARATOR)) == len(script_level.split(LEVEL_SEPARATOR))
            # Le script doit correspondre exactement au niveau actuel ou à un sous-niveau direct
            # print(f'startswith({level_prefix}) and len({level_prefix}.split(LEVEL_SEPARATOR)) == len({script_level}.split(LEVEL_SEPARATOR)) cond={cond}' )
            if( cond ): 
                #script_type = "sh" if file_name.endswith(".sh") else "py"
                script_type = Path(file_name).suffix[1:]
                scripts.append((file_name, description, script_level, script_type))

    # Trier les scripts par script_level
    scripts_sorted = sorted(scripts, key=lambda x: x[2])  # x[2] correspond à script_level

    return scripts_sorted

def display_menu(scripts, is_root):
    """Affiche le menu des scripts disponibles"""
    for i, (file_name, description, _, script_type) in enumerate(scripts):
        command = Path(file_name).stem
        if description:  # N'affiche que si la description est présente
            print(f"{i} - {command} - {description} ({script_type})")
        else:
            print(f"{i} - {command} ")

    # Affiche "quit" au niveau 0, sinon "retour"
    print(f"{len(scripts)} - {'quit' if is_root else 'retour'}")


def execute_script(script_name):
    """Exécute le script choisi par l'utilisateur"""
    input("\nAppuyez sur Entrée pour executer...")
    print(f"Exécution de {script_name}...\n")
    os.system(f"./{script_name}")
    input("\nAppuyez sur Entrée pour continuer...")

def handle_exit(signal_received, frame):
    """Gestionnaire pour les interruptions Ctrl+C"""
    choice = input("\nVoulez-vous vraiment quitter [Y/n] ? ")
    if choice.lower() in ['y', '']:
        sys.exit(0)

def main():
    # Attacher le signal Ctrl+C à la fonction handle_exit
    #signal.signal(signal.SIGINT, handle_exit)
    
    previous_level = None
    current_level = ""
    while True:
        try:
            load_header()

            is_root = (current_level == "")
            scripts = parse_scripts(current_level, is_root)

            if not scripts:
                print("Aucun script disponible pour ce niveau.")
                if is_root:
                    input("\nAppuyez sur Entrée pour sortir...")
                    break

            display_menu(scripts, is_root)
            choice = input("Choisissez une option: ")

            if choice.isdigit() and int(choice) < len(scripts):
                script_name, _, script_level, _ = scripts[int(choice)]
                # Ajouter un niveau pour descendre dans la hiérarchie
                next_level = script_level + LEVEL_SEPARATOR

                next_scripts = parse_scripts(next_level, next_level == "") 
                if next_scripts and scripts != next_scripts:
                    current_level = next_level
                else:
                    execute_script(script_name)
                    # Retour à l'écran précédent après exécution
            elif choice == str(len(scripts)):
                if is_root:
                    # Demander la confirmation avant de quitter
                    handle_exit(None, None)
                else:
                    # Revenir au niveau supérieur
                    current_level = LEVEL_SEPARATOR.join(current_level.split(LEVEL_SEPARATOR)[:-1])[:-1]
            else:
                print("Choix invalide, veuillez réessayer.")
        except KeyboardInterrupt:
            handle_exit(None, None)

if __name__ == "__main__":
    main()

