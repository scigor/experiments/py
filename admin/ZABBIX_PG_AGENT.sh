#!/bin/bash
#
#LEVEL: 0_3
#DESC: install Zabbix Postgres + agent configure

sudo apt update -qq 2&>1 > /dev/null
apt install net-tools

apt install -y postgresql
pg_ctlcluster 14 main  start

sudo -u postgres
sudo -u postgres psql -c "create database zabbix;"
sudo -u postgres psql -c "create user admin with password 'zteam';"
sudo -u postgres psql -c "grant ALL privileges on database zabbix to admin;"

echo "listen_addresses = ' *'"                  >> /etc/postgresql/14/main/postgresql.conf
echo "host zabbix admin 163.172.170.222/32 md5" >> /etc/postgresql/14/main/pg_hba.conf

cat >> /etc/zabbix/zabbix_agentd.conf <<EOF
HostnameItem=system.hostname
HostMetadataItem=system.uname
EOF

cat > /etc/zabbix/zabbix_agentd.d/user_parameter_sleep.conf <<EOF
UserParameter=ps.sleep,ps aux | grep [s]leep | wc -l 2>/dev/null
EOF

systemctl restart postgresql
zabbix_agentd -R userparameter_reload
