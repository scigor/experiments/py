#!/bin/bash
#
#LEVEL: 0_2
#DESC: install Zabbix

sudo apt update -qq 2&>1 > /dev/null
apt install net-tools

wget https://repo.zabbix.com/zabbix/6.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-4+ubuntu22.04_all.deb
sudo dpkg -i zabbix-release_6.0-4+ubuntu22.04_all.deb

sudo apt update -qq 2&>1 > /dev/null

apt install -y zabbix-server-pgsql zabbix-frontend-php php8.1-pgsql zabbix-nginx-conf zabbix-sql-scripts zabbix-agent postgresql-client

zcat /usr/share/zabbix-sql-scripts/postgresql/server.sql.gz |PGPASSWORD="zteam" psql -h zteam-master02-zpg1 -U admin zabbix

sed -i s/"#.*server_name.*"/"        server_name zteam-master02-z1.zteam.fr; "/g /etc/zabbix/nginx.conf

echo "DBPassword=zteam" >> /etc/zabbix/zabbix_server.conf
echo "DBHost=zteam-master02-zpg1" >> /etc/zabbix/zabbix_server.conf

systemctl restart zabbix-server zabbix-agent nginx php8.1-fpm
systemctl enable zabbix-server zabbix-agent nginx php8.1-fpm
