sudo apt install docker.io -y

sudo systemctl enable docker
sudo systemctl status docker

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg	
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update

sudo apt install kubeadm kubelet kubectl
sudo apt-mark hold kubeadm kubelet kubectl

cat >> /etc/modules-load.d/containerd.conf <<-EOF
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat >> /etc/sysctl.d/kubernetes.conf <<-EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

#echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf
#sudo sysctl -p

#echo "net.ipv6.conf.all.forwarding=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf
#sudo sysctl -p

sudo sysctl --system
sudo hostnamectl set-hostname  $(hostname)

cat >> /etc/default/kubelet <<-EOF
KUBELET_EXTRA_ARGS="--cgroup-driver=cgroupfs"
EOF
sudo systemctl daemon-reload && sudo systemctl restart kubelet && sudo systemctl status kubelet


cat >> /etc/docker/daemon.json <<-EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
  "max-size": "100m"
  },
   "storage-driver": "overlay2"
}
EOF
sudo systemctl daemon-reload && sudo systemctl restart docker & sudo systemctl status  docker

## 
add  /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf 
Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"

sudo kubeadm init --control-plane-endpoint=<kube-balancer-ip>:6443 --upload-certs

kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
Or
kubectl apply -f https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml
kubectl taint nodes --all node-role.kubernetes.io/control-plane-


kubeadm token create --print-join-command

