from scaleway import Client
from scaleway.instance.v1 import InstanceV1API
import discord
from discord.ext import commands

# Configuration des credentials
ACCESS_KEY = 'SCWG7H5Y9H5JF8KA5MF9'
SECRET_KEY = '7cf87392-45cf-4612-a0e7-b0076ca5b13a'
DEFAULT_PROJECT_ID = 'ae7900cc-f75e-4f05-baa3-525295ce4c32'

# Initialiser le client Scaleway
client = Client(
    access_key=ACCESS_KEY,
    secret_key=SECRET_KEY,
    default_project_id=DEFAULT_PROJECT_ID,
    default_region="fr-par",
    default_zone="fr-par-1"
)

# Acceder a l'API Compute
instance_api = InstanceV1API(client)

# Creer un objet Intents avec uniquement les intents necessaires
intents = discord.Intents.default()
intents.message_content = True  # Activer uniquement l'intent pour lire les messages

# Initialiser le bot avec les intents
bot = commands.Bot(command_prefix="!", intents=intents)

@bot.event
async def on_ready():
    print(f'Nous sommes connectés en tant que {bot.user}')

@bot.command()
async def tcg_on(ctx):
    try:
        server_id = get_server_id_by_name('zteam-dev05')
        if server_id:
            # Utilisation correcte de la méthode server_action
            instance_api.server_action(
                server_id=server_id,
                action='poweron'
            )
            await ctx.send(f"Le serveur {server_id} a été démarré avec succès.")
        else:
            await ctx.send(f"Serveur {server_id} introuvable.")
    except Exception as e:
        await ctx.send(f"Erreur lors du démarrage du serveur : {e}")

@bot.command()
async def tcg_off(ctx):
    try:
        server_id = get_server_id_by_name('zteam-dev05')
        if server_id:
            instance_api.server_action(
                server_id=server_id,
                action='poweroff'
            )
            await ctx.send(f"Le serveur {server_id} a été stoppé avec succès.")
        else:
            await ctx.send(f"Serveur {server_id} introuvable.")
    except Exception as e:
        await ctx.send(f"Erreur lors de l'arrêt du serveur : {e}")

def get_server_id_by_name(server_name):
    # Implémentez ici la logique pour obtenir l'ID du serveur basé sur son nom
    if server_name == 'zteam-dev05':
        return 'c19bf972-16ac-48db-ac7a-818d81e58a29'
    return None

bot.run('MTI3OTE3MDI2NTE4MzQyNDY0NQ.GuNeS1.aXEdGX0mdNoe4juhQQcsnEZCC7KhxVhGHCnOPY')  # Remplacez par votre vrai token

