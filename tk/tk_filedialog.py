#!/usr/bin/env python
# coding: utf-8

import tkinter as tk

from tkinter import filedialog

class Image(tk.Tk):    
    def __init__(self, x, y):    
        super().__init__()
        self.title("main")
        
        filepath = filedialog.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])    
        self.img_file = tk.PhotoImage(file=filepath)

        canvas = tk.Canvas(self, width=500, height=500)
        canvas.pack(fill=tk.BOTH)

        self.image = canvas.create_image(x,y, image=self.img_file)
        self.focus_set()

if __name__ == '__main__':
    app = Image(8, 8)
    app.mainloop()
        


