# coding: utf-8
import tkinter as tk
from tkinter import filedialog
from tkinter import Toplevel
from tkinter import ttk

from tkinter.messagebox import *
from tkinter.filedialog import *

from PIL import Image, ImageTk

from tkinter import ALL, EventType

class App(tk.Tk):

    def alert(self):
        showinfo("alerte", "Bravo!")

    #def do_zoom(event):
     #   x = canvas.canvasx(event.x)
      #  y = canvas.canvasy(event.y)
       # factor = 1.001 ** event.delta
        #canvas.scale(ALL, x, y, factor, factor)

    def __init__(self):
        super().__init__()
        self.title("zspritesheet")
        self.geometry("620x300")
        #self.configure(background='red')

        self.textApp = tk.StringVar()
        self.textApp.set("Welcome")

        menubar = tk.Menu(self)

        menu1 = tk.Menu(menubar, tearoff=0)
        menu1.add_command(label="Import", command=self.importer)
        menu1.add_command(label="Export", command=self.alert)
        menu1.add_separator()
        menu1.add_command(label="Quitter", command=self.quit)
        menubar.add_cascade(label="Fichier", menu=menu1)

        menu2 = tk.Menu(menubar, tearoff=0)
        menu2.add_command(label="Couper", command=self.alert)
        menu2.add_command(label="Copier", command=self.alert)
        menu2.add_command(label="Coller", command=self.alert)
        menubar.add_cascade(label="Editer", menu=menu2)

        menu3 = tk.Menu(menubar, tearoff=0)
        menu3.add_command(label="A propos", command=self.alert)
        menubar.add_cascade(label="Aide", menu=menu3)

        self.config(menu=menubar)

        frame2 = tk.Frame(self, background='grey')

        self.tree = ttk.Treeview(frame2)
        self.tree.heading('#0', text='SpriteSheet', anchor=tk.W)

        self.tree.pack(side=tk.LEFT, expand=tk.N, fill=tk.BOTH)

        self.canvas = tk.Canvas(frame2,bg='black', width="320", height="220" )
        self.canvas.pack(side=tk.TOP,anchor=tk.NW)

        frame2.pack(expand=tk.Y, fill=tk.BOTH)

        self.tabDetails = ttk.Notebook(self)
        self.frameProps = ttk.Frame(self.tabDetails)
        self.frameAttrs = ttk.Frame(self.tabDetails)
        self.tabDetails.add(self.frameProps, text='Properties')
        self.tabDetails.add(self.frameAttrs, text='Attributes')
        self.tabDetails.pack(expand=tk.Y, fill=tk.BOTH)

        labelimg = tk.Label(self.frameProps, textvariable=self.textApp, anchor=tk.CENTER).pack(side=tk.LEFT,padx=10, pady=10)

    def split(self):
        self.option.destroy()

        for item in self.tree.get_children():
            self.tree.delete(item)

        self.textApp.set(self.filepath)
        # adding data
        
        iid_parent=0;iid=0;
        h = 4;#int(self.img_file.height) / int(self.img_file.height);##self.cell_width;
        w = 4;#int(self.img_file.width) / int(self.img_file.width);##self.cell_width;

        for i in range(h):
         iid_parent = iid;
         self.tree.insert('', tk.END, text='Anim'+str(i), iid=iid, open=False);
         iid+=1;
         for j in range(w):
          self.tree.insert('', tk.END, text='Frame'+str(j), iid=iid, open=False)
          self.tree.move(iid, iid_parent, j)
          iid+=1;
        
        self.tree.pack(side=tk.LEFT, expand=tk.N, fill=tk.BOTH)

        self.canvas.pack_forget()
        self.canvas.create_image(50, 10, image=self.img_file, anchor=tk.NW)
        self.canvas.create_image(70, 10, image=self.img_file, anchor=tk.NW)
        #innercanvas = tk.Canvas(self.canvas,width=50, height=10)
        #innercanvas.create_image(50, 10, image=self.img_file, anchor=tk.NW)

        #sp.bind("<MouseWheel>", do_zoom)
        self.canvas.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.canvas.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))

        self.canvas.pack(side=tk.TOP,anchor=tk.NW)

    def importer(self):
        self.option = Toplevel(self, bg="black")
        self.option.title("Importer")
        
        self.filepath = filedialog.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])    
        #self.image = Image.open(self.filepath)
        #self.img_file = ImageTk.PhotoImage(self.image)
        self.img_file = tk.PhotoImage(file=self.filepath)
        
        label = tk.Label(self.option, text=self.filepath)
        label.pack()

        label2 = tk.Label(self.option, image=self.img_file)
        label2.pack()

        frameAction = tk.Frame(self.option) #, background='white')
        tk.Button(frameAction ,text="Importer", command=self.split).pack(side=tk.LEFT)
        tk.Button(frameAction ,text="Annuler", command=self.option.destroy).pack(side=tk.LEFT)
        frameAction.pack()

        self.option.grab_set()
        self.option.focus_force()


if __name__ == '__main__':
    app = App()
    app.mainloop()
