#!/usr/bin/env python3

import time
from datetime import datetime

while True:
  with open("/tmp/timestamp.txt", "a", encoding="utf-8") as f:
    f.write("The current timestamp is {}\n".format(datetime.now()))
  time.sleep(10)
