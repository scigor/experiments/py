#!/usr/bin/env python3

import os
import signal
import sys

import time
from datetime import datetime

import argparse

name = 'testd'
interval = 5

#class TestDaemon():

#  def __init__(self, name, interval):
#    self.name = name;
#    self.interval = interval

def signal_handler(signum, frame):
    os.remove(f"/tmp/{name}.pid")
    print("Process terminated gracefully")
    sys.exit(0)

def write_pid(pid):
    with open(f"/tmp/{name}.pid", "w") as f:
      f.write(str(pid))

def read_pid():
    try:
      with open(f"/tmp/{name}.pid", "r") as f:
        return int(f.read().strip())
    except FileNotFoundError:
      return None

def start():
    #try:
      print(f"Starting background {name}...")

      while True:

        ##print(f"Process every {interval} minutes")

        with open(f"/tmp/{name}.log", "a", encoding="utf-8") as f:
          f.write("The current timestamp is {}\n".format(datetime.now()))

        time.sleep(interval)  # Pause for XX seconds
    #except KeyboardInterrupt:
    #  pass

def start_detached_process():
    if os.fork() == 0:  # Create a child process
        # Detach child process
        os.setsid()
        p = Process(target=start)
        p.start()
        write_pid(p.pid)
        # Exit the intermediate child
        sys.exit(0)

def stop():
    pid = read_pid()
    if pid is None:
      print("No running process found.")
      return
    try:
      os.kill(pid, signal.SIGTERM)
      os.remove(f"/tmp/{name}.pid")
      print(f"Stopped the process with PID {pid}")
    except ProcessLookupError:
      print("Process already stopped or PID file is stale.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Start or stop a background process.")
    parser.add_argument("command", choices=['start', 'stop'], help="Command to start or stop the process")

    args = parser.parse_args()

    if args.command == "start":
      ## Set up a signal handler to handle graceful shutdowns
      signal.signal(signal.SIGTERM, lambda signum, frame: sys.exit(0))
      #start()
      start_detached_process()
    elif args.command == "stop":
      stop()
