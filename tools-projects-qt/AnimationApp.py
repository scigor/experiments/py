import sys
import requests
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QComboBox, QPushButton, QListWidget

class AnimationApp(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()
        
        self.selected_actor = None
        self.selected_state = None
        self.selected_file = None
        self.actors = []
        self.states = []
        self.frames = []

    def initUI(self):
        self.setWindowTitle("Animation App")
        
        # Layout principal
        layout = QVBoxLayout()
        
        # Actor selection
        self.actor_label = QLabel("Select Actor:", self)
        layout.addWidget(self.actor_label)
        self.actor_combobox = QComboBox(self)
        self.actor_combobox.currentIndexChanged.connect(self.on_actor_selected)
        layout.addWidget(self.actor_combobox)

        # State selection
        self.state_label = QLabel("Select State:", self)
        layout.addWidget(self.state_label)
        self.state_combobox = QComboBox(self)
        self.state_combobox.currentIndexChanged.connect(self.on_state_selected)
        layout.addWidget(self.state_combobox)

        # Frame selection
        self.frame_label = QLabel("Select Frame:", self)
        layout.addWidget(self.frame_label)
        self.frames_list = QListWidget(self)
        layout.addWidget(self.frames_list)

        # Load Data Button
        self.load_button = QPushButton("Load Data", self)
        self.load_button.clicked.connect(self.load_data)
        layout.addWidget(self.load_button)
        
        # Set layout
        self.setLayout(layout)
        
        # Fetch actors on startup
        self.fetch_actors()

    def fetch_actors(self):
        """Récupère la liste des acteurs depuis le backend."""
        try:
            response = requests.get('http://localhost:3001/api/actors')
            actors = response.json()
            self.actors = actors
            self.actor_combobox.clear()
            self.actor_combobox.addItems(actors)
        except Exception as e:
            print(f"Error fetching actors: {e}")

    def on_actor_selected(self):
        """Gestion de la sélection d'un acteur."""
        self.selected_actor = self.actor_combobox.currentText()
        self.fetch_states()

    def fetch_states(self):
        """Récupère la liste des états d'un acteur sélectionné."""
        if not self.selected_actor:
            return
        try:
            response = requests.get(f'http://localhost:3001/api/states/{self.selected_actor}')
            states = response.json()
            self.states = states
            self.state_combobox.clear()
            self.state_combobox.addItems(states)
        except Exception as e:
            print(f"Error fetching states: {e}")

    def on_state_selected(self):
        """Gestion de la sélection d'un état."""
        self.selected_state = self.state_combobox.currentText()
        self.fetch_files()

    def fetch_files(self):
        """Récupère la liste des fichiers (frames) pour l'état sélectionné."""
        if not self.selected_actor or not self.selected_state:
            return
        try:
            response = requests.get(f'http://localhost:3001/api/files/{self.selected_actor}/{self.selected_state}')
            files = response.json()
            self.frames = files
            self.frames_list.clear()
            self.frames_list.addItems([f"Frame {i+1}: {file}" for i, file in enumerate(files)])
        except Exception as e:
            print(f"Error fetching files: {e}")

    def load_data(self):
        """Fonction de chargement des données (à étendre si nécessaire)."""
        print("Data Loaded:")
        print(f"Actor: {self.selected_actor}")
        print(f"State: {self.selected_state}")
        print(f"Files: {self.frames}")

def main():
    app = QApplication(sys.argv)
    ex = AnimationApp()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

