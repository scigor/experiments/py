#!/usr/bin/env python3

import xmltodict
import json

dict_data = xmltodict.parse("""
    <Paris>
    <Hotel>
        <ID>1</ID>
        <Name>Les Jardins du Marais</Name>
        <Star>3</Star>
        <Facilities>Internet</Facilities>
        <Available>True</Available>
    </Hotel>
    <Hotel>
        <ID>2</ID>
        <Name>Golden Tulip Little Palace</Name>
        <Star>4</Star>
        <Facilities>
             <facility1>Internet</facility1>
             <facility2>Gym</facility2>
             <facility3>Parking</facility3>
             <facility4>Restaurant</facility4>
        </Facilities>
        <Available>False</Available>
    </Hotel>
</Paris>
""")
print(json.dumps(dict_data))
print

# opening the xml file
with open("data.xml","r") as xmlfileObj:
    #converting xml data to dictionary
    data_dict = xmltodict.parse(xmlfileObj.read())
    xmlfileObj.close()
    #creating JSON object using dictionary object  
    jsonObj= json.dumps(data_dict)
   
    #storing json data to json file
    with open("data.json", "w") as jsonfileObj:
        jsonfileObj.write(jsonObj)
        jsonfileObj.close()
