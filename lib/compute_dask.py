import dask.dataframe as dd
import pandas as pd
import time
from dask.distributed import Client

import psutil

cpu_info = {
    'physical_cores': psutil.cpu_count(logical=False),
    'total_cores': psutil.cpu_count(logical=True),
    'max_frequency': psutil.cpu_freq().max,
    'min_frequency': psutil.cpu_freq().min,
    'current_frequency': psutil.cpu_freq().current,
    'cpu_usage': psutil.cpu_percent(interval=1)
}

print(cpu_info)

meta = {
    'nom_machine': 'object',
    'timestamp': 'datetime64[ns]',
    'statut': 'object',
    'jour':'int32',
    'mois':'int32',
    'semaine':'UInt32',
    'année':'int32',
    'heure':'int32',
    'is_weekend':'bool',
    'computeStatut':'object'
}

# Lire le fichier CSV
ddf = dd.read_csv('input.csv')

start = time.time()
# Convertir la colonne de timestamp en datetime
ddf['timestamp'] = dd.to_datetime(ddf['timestamp'], format='%b %Y %d, %H:%M:%S')

# Extraire les informations du timestamp
ddf['jour'] = ddf['timestamp'].dt.day
ddf['mois'] = ddf['timestamp'].dt.month
ddf['semaine'] = ddf['timestamp'].dt.isocalendar().week
ddf['année'] = ddf['timestamp'].dt.year
ddf['heure'] = ddf['timestamp'].dt.hour

ddf['is_weekend'] = ddf['timestamp'].dt.dayofweek.isin([5, 6])

# Définir la fonction de transformation
def determine_compute_statutA(df):
    df['is_C_next_weekend'] = (
        df.groupby('nom_machine')['statut']
        .shift(-1)
        .eq('C') & 
        df['timestamp'].dt.dayofweek.isin([5, 6])
    )
    
    df['computeStatut'] = df.apply(
        lambda row: 'C' if row['is_C_next_weekend'] else row['statut'], axis=1
    )
    
    #return df.drop(columns=['is_C_next_weekend'])
    return df[['nom_machine', 'timestamp', 'statut', 'jour','mois','semaine','année','heure','computeStatut']]

# Définir une fonction pour marquer les lignes avec 'C' le weekend suivant
def mark_compute_statut(partition):
    # Grouper par nom_machine et semaine
    grouped = partition.groupby(['nom_machine', 'semaine'])
    
    # Trouver les machines où 'C' est présent le weekend suivant
    machines_with_c_next_weekend = grouped.apply(
        lambda group: group[group['is_weekend'] & (group['statut'] == 'C')], include_groups=False
        #,meta=partition._meta
    )
    
    # Identifier les indices de ces lignes
    indices_with_c_next_weekend = machines_with_c_next_weekend.index.get_level_values(1).tolist()
    
    # Marquer computeStatut à 'C' pour les lignes concernées
    partition.loc[indices_with_c_next_weekend, 'computeStatut'] = 'C'
    
    # Retourner la partition modifiée
    return partition

# Définir la fonction de transformation
def determine_compute_statut_pandas(row, df):
    machine = row['nom_machine']
    semaine_courante = row['semaine']
    année_courante = row['année']
    
    weekend_suivant = df[
        (df['nom_machine'] == machine) & 
        (df['année'] == année_courante) & 
        (df['semaine'] == semaine_courante + 1) & 
        (df['timestamp'].dt.dayofweek >= 5)
    ]
    
    if 'C' in weekend_suivant['statut'].values:
        return 'C'
    else:
        return row['statut']

def apply_determine_compute_statut(df):
    df['computeStatut'] = df.apply(determine_compute_statut_pandas, axis=1, args=(df,))
    return df

# Appliquer la fonction à chaque partition

#ddf['computeStatut'] = ddf.map_partitions(lambda partition: partition.apply(determine_compute_statutA, axis=1, args=(partition,)))
ddf = ddf.map_partitions(mark_compute_statut, meta=meta)
#ddf = ddf.drop(columns=['is_weekend'])

#ddf = ddf.map_partitions(determine_compute_statutA, meta=meta)
#ddf = ddf.map_partitions(apply_determine_compute_statut, meta=meta)

# Écrire le DataFrame complet dans un fichier CSV
ddf.to_csv('output_complet_dask.csv', single_file=True, index=False)

end = time.time()

# Afficher le DataFrame résultant
print(f"time is {end - start}")

# Filtrer les lignes où computeStatut est différent de statut
filtered_df = ddf[ddf['computeStatut'] != ddf['statut']]

# Obtenir les 5 premiers hôtes où computeStatut est différent de statut
first_5_hosts = filtered_df['nom_machine']

# Convertir en Pandas Series pour affichage des premières valeurs
first_5_hosts = filtered_df.compute()

# Sélectionner les 5 premières valeurs de la Series Pandas
first_5_hosts = first_5_hosts.head(5)

# Afficher les résultats
print("Les 5 premiers hôtes où computeStatut est différent de statut :")
print(first_5_hosts)