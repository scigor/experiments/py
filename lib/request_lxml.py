#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

from lxml.html import fromstring

import lxml.html as lh

from io import StringIO, BytesIO

import unittest, time, re
import getopt, argparse, sys

import requests
import base64

import logging
import textwrap

#from fake_useragent import UserAgent
#from requests.auth import HTTPBasicAuth

#ua = UserAgent()
#print(ua.chrome)

#logging.basicConfig()
logging.basicConfig(level=logging.DEBUG)

#logging.getLogger().setLevel(logging.DEBUG)

#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.INFO)
#requests_log.propagate = True

from bs4 import BeautifulSoup

def print_status(response, *args, **kwargs):
 format_headers = lambda d: '\n'.join(f'{k}: {v}' for k, v in d.items())
 print(textwrap.dedent('''
    ---------------- request ----------------
    {req.method} {req.url} 
    ---------------- response ----------------
    {res.status_code} {res.reason} {res.url}

 ''').format(
    req=response.request, 
    res=response,
 ))


def print_info(response, *args, **kwargs):
 format_headers = lambda d: '\n'.join(f'{k}: {v}' for k, v in d.items())
 print(textwrap.dedent('''
    ---------------- request ----------------
    {req.method} {req.url} 
    {reqhdrs}

    ---------------- response ----------------
    {res.status_code} {res.reason} {res.url}
    {reshdrs}

 ''').format(
    req=response.request, 
    res=response, 
    reqhdrs=format_headers(response.request.headers), 
    reshdrs=format_headers(response.headers), 
 ))

def print_data(response, *args, **kwargs):
 format_headers = lambda d: '\n'.join(f'{k}: {v}' for k, v in d.items())
 print(textwrap.dedent('''
    ---------------- response ----------------
    {res.status_code} {res.reason} {res.url}

  {res.content}
 ''').format(
    res=response
 ))

class BookView:
#class BookView:(unittest.TestCase):

    url = 'http://scaleway.com/bookStore'
    username = "user"
    password = "password!"
    book = "py"
    author = "scigor"

    sess = requests.Session()
    resp = requests.Response()

    def setUp(self):

     self.getoptions()
     self.verificationErrors = []
     self.accept_next_alert = True
     
     #self.getOptions()

    def __init__(self):

     self.getoptions()

     self.verificationErrors = []
     self.accept_next_alert = True

     self.sess = requests.Session()
     self.resp = requests.Response()

    def test_app(self):

     session = self.sess
     print(self.book+" "+self.author+" "+self.environment+" "+self.baseline+"\n");
     header = {'Content-Type': 'application/x-www-form-urlencoded', 'Connection':'keep-alive', 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36' } #, 'User-Agent':str(ua.chrome)
     proxy = {'http': 'http://'+self.username+':'+self.password+'@proxy-rc:9090'}

     resp = session.get(self.url)
     view = fromstring(resp.content).xpath("//input[@name='com.sun.faces.VIEW']/@value")

     resp = session.post(self.url+"faces/pages/login.jsp",
       data={
        'com.sun.faces.VIEW':str(view[0]),
        'loginForm':'loginForm',
        'loginForm:username':self.username,
        'loginForm:password':self.password,
       },
       headers=header, cookies=resp.cookies, allow_redirects=True, hooks={'response': print_status}
       #headers=dict(Referer=self.url), cookies=resp.cookies, allow_redirects=False
     )
     view = fromstring(resp.content).xpath("//input[@name='com.sun.faces.VIEW']/@value")
      
     resp = session.post(self.url+"faces/pages/main.jsp",
       data={
        'initForm':'initForm',
        'initForm:initForm:authorListBox':'',
        'initForm:initForm:authorInput':'',
        'initForm:initForm:bookListBox':'',
        'initForm:initForm:bookInput':'',
        'com.sun.faces.VIEW':str(view[0])
       },
       headers=header, cookies=resp.cookies, allow_redirects=True, hooks={'response': print_status}
     )
     view = fromstring(resp.content).xpath("//input[@name='com.sun.faces.VIEW']/@value")
     
     resp = session.post(self.url+"faces/pages/search.jsp",
       data={
        'searchForm':'searchForm',
        'searchForm:search:bookInput':self.book+'%',
        'searchForm:search:authorInput':self.author+'%',
        'searchForm:search:pageInput':'1',
        'com.sun.faces.VIEW':str(view[0])
       },
       headers=header, cookies=resp.cookies, allow_redirects=True, hooks={'response': print_data}
     )
     view = fromstring(resp.content).xpath("//input[@name='com.sun.faces.VIEW']/@value")
     
     # depend on authorListBox / authorInput
     id_books = fromstring(resp.content).xpath('//*[@id="searchForm:search:authorListBox"]/option/text()')
     
     id_pages = fromstring(resp.content).xpath("count(//*[@id='searchForm:pageListBox']/option)")
     id_lines = fromstring(resp.content).xpath("count(//*[@id='searchForm:search']/tbody/tr)")
     id_lines_total = 0
     
     print('books='+str(len(id_books))+',  pages='+str(int(id_pages))+', lines='+str(int(id_lines)))
     
     #
     # LOOP
     
     f = open('extract_'+self.book+'.csv', 'w')
     try:
      #if( id_pages>0):
       for id_page in range(0, int(id_pages)):
       
        resp = session.post(self.url+"faces/pages/search.jsp",
          data={
           'searchForm':'searchForm',
           'searchForm:search:bookInput':self.book+'%',,
           'searchForm:search:authorInput':self.author+'%',
           'searchForm:search:pageInput':str(id_page+1),
           'com.sun.faces.VIEW':str(view[0])
          },
          headers=header, cookies=resp.cookies, allow_redirects=True, hooks={'response': print_data}
        )
        view = fromstring(resp.content).xpath("//input[@name='com.sun.faces.VIEW']/@value")      
        # 
        #  #pretty = ""
        #  #soup = BeautifulSoup(resp.content, 'html.parser')
        #  #soup = BeautifulSoup(resp.content, features="lxml")
        #  #for value in soup.find_all("td"):
        #  # pretty += value.prettify()
        #  #print(str(livraison))
        # 
        #  #doc=fromstring(resp.content)
        #  #for div in doc.cssselect('tr.complementBgOddTableau:nth-child(1) > td:nth-child(5)'):
        #  # print(div.text_content())
        # 
        id_lines = fromstring(resp.content).xpath("count(//*[@id='searchForm:search']/tbody/tr)")
        id_lines_total+=int(id_lines)
        id_line = 0
        for k in range(0, int(id_lines)):
         id_line = id_line + 1
         book = fromstring(resp.content).cssselect('tbody > tr:nth-child('+str(id_line)+') > td:nth-child(3)')[0].text_content()
         author = fromstring(resp.content).cssselect('tbody > tr:nth-child('+str(id_line)+') > td:nth-child(4)')[0].text_content()
         id = fromstring(resp.content).cssselect('tbody > tr:nth-child('+str(id_line)+') > td:nth-child(5)')[0].text_content()
         f.write(str(book)+";"+str(author)+"\n")
        
        print('############################## id_line='+str(id_line)+', pages='+str(int(id_pages)) + ', lines='+str(int(id_lines_total)))
       
     except Exception as err:
      print(err)
     finally:
      f.close()

    def is_element_present(self, how, what):
     try: self.driver.find_element(by=how, value=what)
     except NoSuchElementException as e: return False
     return True

    def is_alert_present(self):
     try: self.driver.switch_to_alert()
     except NoAlertPresentException as e: return False
     return True

    def close_alert_and_get_its_text(self):
     try:
      alert = self.driver.switch_to_alert()
      alert_text = alert.text
      if self.accept_next_alert:
       alert.accept()
      else:
       alert.dismiss()
      return alert_text
     finally: self.accept_next_alert = True

    def tearDown(self):
        # To know more about the difference between verify and assert,
        # visit https://www.seleniumhq.org/docs/06_test_design_considerations.jsp#validating-results
        self.assertEqual([], self.verificationErrors)

    def getoptions(self):
     try:
      opts, args = getopt.getopt(sys.argv[1:], "u:p:b:a:", ["user=", "pass=", "author=", "book=" ])
      
      for o, a in opts:
       if o in ("-u", "--user"):
        self.user = a
       elif o in ("-p", "--pass"):
        self.password = a
       elif o in ("-a", "--author"):
        self.author = a
       elif o in ("-b", "--book"):
        self.book = a

       #else:
        #usage()
        #assert False, "unhandled option"

      #parser = argparse.ArgumentParser(
      # description='Description of what the module does when run.')
      #parser.add_argument("-u", "--user", help='user Login.')
      #parser.add_argument("-p", "--password", help='pass Login.')
      #parser.add_argument("-a", "--author", help='author.')
      #parser.add_argument("-b", "--book", help='book.')
      #args = parser.parse_args()

     except getopt.GetoptError as err:
      # print help information and exit:
      print(str(err))  # will print something like "option -a not recognized"
      sys.exit()

if __name__ == "__main__":

   BookView().test_app()
