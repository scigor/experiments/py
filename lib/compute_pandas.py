import pandas as pd
import time
import psutil

cpu_info = {
    'physical_cores': psutil.cpu_count(logical=False),
    'total_cores': psutil.cpu_count(logical=True),
    'max_frequency': psutil.cpu_freq().max,
    'min_frequency': psutil.cpu_freq().min,
    'current_frequency': psutil.cpu_freq().current,
    'cpu_usage': psutil.cpu_percent(interval=1)
}

print(cpu_info)

start = time.time()

# Lire le fichier CSV
df = pd.read_csv('input.csv')

# Convertir la colonne de timestamp en datetime
df['timestamp'] = pd.to_datetime(df['timestamp'], format='%b %Y %d, %H:%M:%S')

# Extraire les informations du timestamp
df['jour'] = df['timestamp'].dt.day
df['mois'] = df['timestamp'].dt.month
df['semaine'] = df['timestamp'].dt.isocalendar().week
df['année'] = df['timestamp'].dt.year
df['heure'] = df['timestamp'].dt.hour

# Définir la fonction de transformation
def determine_compute_statut(row, df):
    machine = row['nom_machine']
    semaine_courante = row['semaine']
    année_courante = row['année']
    
    weekend_suivant = df[
        (df['nom_machine'] == machine) & 
        (df['année'] == année_courante) & 
        (df['semaine'] == semaine_courante + 1) & 
        (df['timestamp'].dt.dayofweek >= 5)
    ]
    
    if 'C' in weekend_suivant['statut'].values:
        return 'C'
    else:
        return row['statut']

# Appliquer la fonction
df['computeStatut'] = df.apply(determine_compute_statut, axis=1, args=(df,))

# Écrire le résultat dans un fichier CSV
#df.to_csv('output_complet_pandas.csv', index=False)

end = time.time()

# Afficher le DataFrame résultant
print(f"time is {end - start}")
print(df.dtypes)