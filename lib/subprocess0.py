#!/usr/bin/env python3

import datetime
import subprocess

today = datetime.datetime.now() #datetime.date.today()

print( """
  isoformat=%s
  strftime=%s
  ctime=%s
  """ %(today.isoformat(), today.strftime("%Y-%m-%d %H %M %S"), today.ctime() )
  )
try:
 subprocess.run(["ls", "la"], shell=True, check=True) 
except subprocess.CalledProcessError as e:
 print( e )  
