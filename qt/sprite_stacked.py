import sys
import os
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QHBoxLayout, QWidget, QPushButton, QLineEdit, QFileDialog
from PyQt5.QtGui import QPixmap, QTransform, QPainter
from PyQt5.QtCore import Qt, QTimer

class ImageApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Image Viewer")
        self.setFixedSize(640, 480)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        main_layout = QVBoxLayout(self.central_widget)
        
        button_layout = QHBoxLayout()
        button_layout.setAlignment(Qt.AlignCenter)
        main_layout.addLayout(button_layout)

        self.entry = QLineEdit("1")
        self.entry.setFixedWidth(40)
        button_layout.addWidget(self.entry)

        self.minus_button = QPushButton("-")
        self.minus_button.setFixedSize(40, 40)
        self.minus_button.clicked.connect(self.decrease_id)
        button_layout.addWidget(self.minus_button)

        self.plus_button = QPushButton("+")
        self.plus_button.setFixedSize(40, 40)
        self.plus_button.clicked.connect(self.increase_id)
        button_layout.addWidget(self.plus_button)

        self.toggle_button = QPushButton("Toggle Rotation")
        self.toggle_button.setFixedSize(100, 40)
        self.toggle_button.setCheckable(True)
        self.toggle_button.clicked.connect(self.toggle_rotation)
        button_layout.addWidget(self.toggle_button)

        self.image_display = QWidget()
        main_layout.addWidget(self.image_display)

        self.image_labels = []
        self.current_image_id = 0
        self.rotation_angle = 0
        self.timer = QTimer()
        self.timer.timeout.connect(self.rotate_images)

        self.select_directory()

    def select_directory(self):
        directory = QFileDialog.getExistingDirectory(self, "Select Directory")
        if directory:
            self.load_images(directory)

    def load_images(self, directory):
        for label in self.image_labels:
            label.deleteLater()
        self.image_labels = []

        files = sorted([f for f in os.listdir(directory) if f.endswith('.png') and '-' in f],
                       key=lambda x: int(x.split('-')[-1].split('.')[0]))

        for index, file in enumerate(files):
            path = os.path.join(directory, file)
            pixmap = QPixmap(path).scaled(64, 64, Qt.KeepAspectRatio, Qt.SmoothTransformation)

            # Create a larger pixmap to prevent clipping
            large_pixmap = QPixmap(128, 128)
            large_pixmap.fill(Qt.transparent)
            painter = QPainter(large_pixmap)
            painter.drawPixmap((128 - pixmap.width()) // 2, (128 - pixmap.height()) // 2, pixmap)
            painter.end()

            label = DraggableLabel(self.image_display, large_pixmap)
            label.move(160, 10 + index * 1)  # Décalage vertical de 70 pixels entre chaque image
            label.show()
            self.image_labels.append(label)

        self.current_image_id = 0
        self.entry.setText(str(self.current_image_id + 1))

    def decrease_id(self):
        if self.current_image_id > 0:
            self.current_image_id -= 1
            self.entry.setText(str(self.current_image_id + 1))

    def increase_id(self):
        if self.current_image_id < len(self.image_labels) - 1:
            self.current_image_id += 1
            self.entry.setText(str(self.current_image_id + 1))

    def toggle_rotation(self):
        if self.toggle_button.isChecked():
            self.timer.start(100)
        else:
            self.timer.stop()

    def rotate_images(self):
        self.rotation_angle = (self.rotation_angle + 10) % 360
        for label in self.image_labels:
            #center = label.rect().center()
            #transform = QTransform().translate(center.x(), center.y()).rotate(self.rotation_angle).translate(-center.x(), -center.y())
            #pixmap = label.original_pixmap.transformed(transform, Qt.SmoothTransformation)
            #label.setPixmap(pixmap)
            label.set_rotation_angle(self.rotation_angle)

class DraggableLabel(QLabel):
    def __init__(self, parent, pixmap):
        super().__init__(parent)
        self.original_pixmap = pixmap
        self.setPixmap(pixmap)
        self.rotation_angle = 0
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.offset = None

    def set_rotation_angle(self, angle):
        self.rotation_angle = angle
        self.update()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.offset = event.pos()

    def mouseMoveEvent(self, event):
        if self.offset is not None and event.buttons() == Qt.LeftButton:
            self.move(self.mapToParent(event.pos() - self.offset))

    def mouseReleaseEvent(self, event):
        self.offset = None

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.SmoothPixmapTransform)
        center = self.rect().center()
        transform = QTransform().translate(center.x(), center.y()).rotate(self.rotation_angle).translate(-center.x(), -center.y())
        painter.setTransform(transform)
        draw_x = round(center.x() - self.original_pixmap.width() / 2)
        draw_y = round(center.y() - self.original_pixmap.height() / 2)
        painter.drawPixmap(draw_x, draw_y, self.original_pixmap)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ImageApp()
    window.show()
    sys.exit(app.exec_())
