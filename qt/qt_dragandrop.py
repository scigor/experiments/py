from PyQt5.QtWidgets import QTreeWidget, QApplication, QTreeWidgetItem, QWidget, QVBoxLayout, QSplitter, QTableWidget, QTableWidgetItem
from PyQt5.QtCore import pyqtSignal, Qt

class CustomTreeWidget(QTreeWidget):
    itemDropped = pyqtSignal(QTreeWidgetItem)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setDragDropMode(QTreeWidget.DragDrop)

    def dropEvent(self, event):
        super().dropEvent(event)
        item = self.currentItem()
        if item:
            self.itemDropped.emit(item)


class LayerTab(QWidget):
    def __init__(self, tab_name, parent=None):
        super().__init__(parent)
        self.layout = QVBoxLayout(self)
        self.tree = CustomTreeWidget(self)
        self.tree.setHeaderLabel(tab_name)
        self.layout.addWidget(self.tree)

class LayerTabStandard(LayerTab):
    def __init__(self, tab_name="Objects"):
        super().__init__(tab_name)
        self.splitter = QSplitter(Qt.Vertical)
        self.layout.addWidget(self.splitter)
        self.splitter.addWidget(self.tree)

        self.property_table = QTableWidget(2, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.splitter.addWidget(self.property_table)

        self.tree.currentItemChanged.connect(self.update_properties)
        self.tree.itemDropped.connect(self.handleItemDropped)

    def update_properties(self, current, previous):
        self.property_table.clearContents()
        self.property_table.setRowCount(0)
        if current:
            properties = current.data(0, Qt.UserRole)
            if properties:
                row_count = len(properties)
                self.property_table.setRowCount(row_count)
                for index, (key, value) in enumerate(properties.items()):
                    self.property_table.setItem(index, 0, QTableWidgetItem(key))
                    self.property_table.setItem(index, 1, QTableWidgetItem(value))

    def handleItemDropped(self, item):
        # Logic to handle the item drop event
        print(f"Item dropped: {item.text(0)}")
        # Further logic to create image in a scene can be added here

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    main_widget = LayerTabStandard()
    item1 = QTreeWidgetItem(["Item 1"])
    item2 = QTreeWidgetItem(["Item 2"])
    main_widget.tree.addTopLevelItem(item1)
    main_widget.tree.addTopLevelItem(item2)
    main_widget.show()
    sys.exit(app.exec_())
