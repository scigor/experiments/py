# Version 1.0.1
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QFileDialog, QPushButton, QVBoxLayout, QHBoxLayout, QGridLayout, QMessageBox, QSlider, QComboBox
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QTimer, QEasingCurve

class ImageGrid(QWidget):
    def __init__(self, block_size=16, zoom_factor=4):
        super().__init__()
        self.block_size = block_size
        self.zoom_factor = zoom_factor
        self.image_path = self.open_file_dialog()
        self.mouse_move_active = False  # Variable pour contrôler mouseMoveEvent
        self.num_frames = 30  # Nombre de frames par défaut
        self.easing_type = QEasingCurve.Linear  # Type d'easing par défaut
        self.animation_timer = QTimer(self)  # Ajouter une instance de QTimer
        if self.image_path:
            self.initUI()

    def initUI(self):
        self.setWindowTitle('Image Grid Interaction')
        self.current_button = None
        self.blocks_dict = {}
        self.positions_dict = {}
        self.current_positions_dict = {}

        # Créer un layout principal
        main_layout = QVBoxLayout(self)

        # Ajouter les boutons centrés
        button_layout = QHBoxLayout()
        button_layout.addStretch(1)

        btn1 = QPushButton('Button 1', self)
        btn1.clicked.connect(lambda: self.activate_blocks('btn1'))
        button_layout.addWidget(btn1)

        btn2 = QPushButton('Button 2', self)
        btn2.clicked.connect(lambda: self.activate_blocks('btn2'))
        button_layout.addWidget(btn2)

        btn3 = QPushButton('Button 3', self)
        btn3.clicked.connect(lambda: self.activate_blocks('btn3'))
        button_layout.addWidget(btn3)

        button_layout.addStretch(1)

        main_layout.addLayout(button_layout)

        slider_layout = QHBoxLayout()
        self.frame_slider = QSlider(Qt.Horizontal)
        self.frame_slider.setMinimum(1)
        self.frame_slider.setMaximum(30)
        self.frame_slider.setValue(self.num_frames)
        self.frame_slider.setTickInterval(10)
        self.frame_slider.setTickPosition(QSlider.TicksBelow)
        self.frame_slider.valueChanged.connect(self.slider_value_changed)
        slider_layout.addWidget(QLabel("Frames:"))
        slider_layout.addWidget(self.frame_slider)
        main_layout.addLayout(slider_layout)

        # Ajouter une combobox pour choisir le type d'easing
        easing_layout = QHBoxLayout()
        self.easing_combo = QComboBox()
        self.easing_combo.addItems(['Linear', 'EaseInQuad', 'EaseOutQuad', 'EaseInOutQuad', 'EaseInCubic', 'EaseOutCubic', 'EaseInOutCubic', 'EaseInBounce', 'EaseOutBounce'])
        self.easing_combo.setCurrentIndex(0)  # Sélection par défaut
        self.easing_combo.currentIndexChanged.connect(self.combo_index_changed)
        easing_layout.addWidget(QLabel("Easing Type:"))
        easing_layout.addWidget(self.easing_combo)
        main_layout.addLayout(easing_layout)

        # Créer un widget pour contenir les blocs d'image
        self.image_widget = QWidget(self)
        self.image_layout = QGridLayout(self.image_widget)
        self.image_layout.setSpacing(0)

        # Charger l'image
        self.pixmap = QPixmap(self.image_path)

        # Taille de l'image et calcul du nombre de blocs
        self.img_width = self.pixmap.width()
        self.img_height = self.pixmap.height()
        self.num_blocks_x = self.img_width // self.block_size
        self.num_blocks_y = self.pixmap.height() // self.block_size

        # Initialiser et stocker les positions initiales des blocs pour chaque bouton
        for btn_name in ['btn1', 'btn2', 'btn3']:
            self.store_initial_positions(btn_name)

        # Initialiser la vue avec les blocs du bouton 1
        self.activate_blocks('btn1')

        # Ajouter le widget d'image au layout principal
        main_layout.addWidget(self.image_widget)

        # Taille de la fenêtre (facteur de zoom)
        self.setGeometry(100, 100, self.num_blocks_x * self.block_size * self.zoom_factor,
                         self.num_blocks_y * self.block_size * self.zoom_factor + 150)

    def open_file_dialog(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "Sélectionner un fichier d'image", "", "Images (*.png *.xpm *.jpg)", options=options)
        return file_name

    def store_initial_positions(self, btn_name):
        self.blocks_dict[btn_name] = [[self.create_block_label(x, y) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]
        self.positions_dict[btn_name] = [[(x * self.block_size * self.zoom_factor, y * self.block_size * self.zoom_factor) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]
        self.current_positions_dict[btn_name] = [[(x * self.block_size * self.zoom_factor, y * self.block_size * self.zoom_factor) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]

        # Log initial positions
        print(f"Initial positions stored for {btn_name}: {self.current_positions_dict[btn_name][0][0]}")

    def create_block_label(self, x, y):
        label = QLabel(self)
        block_pixmap = self.pixmap.copy(x * self.block_size, y * self.block_size, self.block_size, self.block_size)
        label.setPixmap(block_pixmap.scaled(self.block_size * self.zoom_factor, self.block_size * self.zoom_factor))
        label.setStyleSheet("border: 1px solid red;")
        
        return label

    def activate_blocks(self, btn_name):
        if self.current_button is not None:
            self.update_current_positions()
        
        self.current_button = btn_name

        # Activer ou désactiver la fonction mouseMoveEvent
        if btn_name in ['btn2', 'btn3']:
            self.mouse_move_active = True
        else:
            self.mouse_move_active = False
            self.animate_blocks('btn2', 'btn3')  # Animation lors du clic sur btn1

        self.update_image_layout()

        # Log after activation
        print(f"After activation of {btn_name}: {self.current_positions_dict[btn_name][0][0]}")

        # Forcer la mise à jour de l'interface utilisateur
        self.image_widget.repaint()
        self.image_widget.update()

    def update_current_positions(self):
        if self.current_button:
            self.current_positions_dict[self.current_button] = [[(label.x(), label.y()) for label in row] for row in self.blocks_dict[self.current_button]]
            
            # Log current positions
            print(f"Updated positions for {self.current_button}: {self.current_positions_dict[self.current_button][0][0]}")

    def update_image_layout(self):
        for i in reversed(range(self.image_layout.count())):
            widget = self.image_layout.itemAt(i).widget()
            self.image_layout.removeWidget(widget)
            widget.setParent(None)

        
        active_blocks = self.blocks_dict[self.current_button]
        for y, row in enumerate(active_blocks):
            for x, label in enumerate(row):
                self.image_layout.addWidget(label, y, x)

        QApplication.processEvents()

        for y, row in enumerate(active_blocks):
            for x, label in enumerate(row):           
                pos = self.current_positions_dict[self.current_button][y][x]
                label.move(*pos)
                label.lower()

        # Forcer la mise à jour de l'interface utilisateur
        self.image_widget.update()
        self.image_widget.repaint()

    def animate_blocks(self, from_btn, to_btn):
        from_positions = self.current_positions_dict[from_btn]
        to_positions = self.current_positions_dict[to_btn]

        # Configurer l'animation avec QTimer pour la boucle
        self.animation_step = 0
        self.animation_timer.timeout.connect(lambda: self.perform_animation_step(from_positions, to_positions))
        self.reset_animation_timer()

    def perform_animation_step(self, from_positions, to_positions):
        t = (self.animation_step + 1) / self.num_frames
        easing_curve = QEasingCurve(self.easing_type)
        eased_t = easing_curve.valueForProgress(t)
        
        for y in range(self.num_blocks_y):
            for x in range(self.num_blocks_x):
                start_pos = from_positions[y][x]
                end_pos = to_positions[y][x]
                new_x = int(start_pos[0] * (1 - eased_t) + end_pos[0] * eased_t)
                new_y = int(start_pos[1] * (1 - eased_t) + end_pos[1] * eased_t)
                self.blocks_dict['btn1'][y][x].move(new_x, new_y)

        self.animation_step += 1
        if self.animation_step >= self.num_frames:
            self.animation_timer.stop()
            #self.animation_step = 0
            #self.reset_animation_timer()

        self.update_current_positions()

        # Forcer la mise à jour de l'interface utilisateur
        self.image_widget.update()
        self.image_widget.repaint()

    def reset_animation_timer(self):
        if self.animation_timer.isActive():
            self.animation_timer.stop()
        self.animation_timer.start(100)

    def slider_value_changed(self, value):
        self.num_frames = value
        print(f"Number of frames set to: {self.num_frames}")

    def combo_index_changed(self, index):
        easing_types = [
            QEasingCurve.Linear, QEasingCurve.InQuad, QEasingCurve.OutQuad, QEasingCurve.InOutQuad,
            QEasingCurve.InCubic, QEasingCurve.OutCubic, QEasingCurve.InOutCubic,
            QEasingCurve.InBounce, QEasingCurve.OutBounce
        ]
        self.easing_type = easing_types[index]
        print(f"Easing type set to: {self.easing_type}")

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.start_x = event.x()
            self.start_y = event.y()

    def mouseMoveEvent(self, event):
        if event.buttons() & Qt.LeftButton and self.mouse_move_active:
            current_x = event.x()
            current_y = event.y()
            delta_x = current_x - self.start_x
            delta_y = current_y - self.start_y
            self.start_x = current_x
            self.start_y = current_y

            active_blocks = self.blocks_dict[self.current_button]
            for col in range(self.num_blocks_x):
                for row in range(self.num_blocks_y):
                    factor = col + 1
                    old_x = active_blocks[row][col].x()
                    new_x = old_x + delta_x * factor
                    max_x = self.num_blocks_x * self.block_size * self.zoom_factor - (self.num_blocks_x - col) * self.block_size * self.zoom_factor
                    if new_x > max_x:
                        new_x = max_x
                    active_blocks[row][col].move(new_x, active_blocks[row][col].y())
                    active_blocks[row][col].lower()

                    factor = row + 1
                    old_y = active_blocks[row][col].y()
                    new_y = old_y + delta_y * factor
                    max_y = self.num_blocks_y * self.block_size * self.zoom_factor - (self.num_blocks_y - row) * self.block_size * self.zoom_factor
                    if new_y > max_y:
                        new_y = max_y
                    active_blocks[row][col].move(active_blocks[row][col].x(), new_y)
                    active_blocks[row][col].lower()

            self.update_current_positions()

            self.image_widget.update()
            self.image_widget.repaint()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ImageGrid()
    ex.show()
    sys.exit(app.exec_())
