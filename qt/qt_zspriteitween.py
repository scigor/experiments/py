import sys
from PyQt5.QtCore import Qt, QPointF, QPropertyAnimation, QEasingCurve, QSequentialAnimationGroup, pyqtProperty, QObject
from PyQt5.QtGui import QColor, QPen, QCursor
from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsLineItem, QGraphicsScene, QGraphicsView, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QApplication

import sys
from PyQt5.QtCore import Qt, QPointF, QPropertyAnimation, QEasingCurve, QSequentialAnimationGroup, pyqtProperty, QObject
from PyQt5.QtGui import QColor, QPen, QCursor
from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsScene, QGraphicsView, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QApplication

import sys
from PyQt5.QtCore import Qt, QPointF, QPropertyAnimation, QEasingCurve, QSequentialAnimationGroup, pyqtProperty, QObject
from PyQt5.QtGui import QColor, QPen, QCursor
from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsScene, QGraphicsView, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QApplication

class AnimatedRect(QGraphicsRectItem):
    def __init__(self, x, y, w, h):
        super().__init__(x, y, w, h)
        self.setBrush(QColor("green"))

class AnimatableItem(QObject):
    def __init__(self, rect_item):
        super().__init__()
        self._rect_item = rect_item

    def getPos(self):
        return self._rect_item.pos()

    def setPos(self, pos):
        self._rect_item.setPos(pos)

    pos = pyqtProperty(QPointF, fget=getPos, fset=setPos)

class PathWindow(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Palette Window")
        self.setFixedSize(400, 300)
        
        # Layout principal
        main_layout = QHBoxLayout(self)
        
        # Layout gauche avec le bouton OK
        left_layout = QVBoxLayout()
        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.on_ok_clicked)
        left_layout.addWidget(self.ok_button)

        self.play_button = QPushButton("Play")
        self.play_button.clicked.connect(self.on_play_clicked)
        left_layout.addWidget(self.play_button)

        left_layout.addStretch()
        
        # Scène et vue graphique
        self.scene = QGraphicsScene()
        self.view = QGraphicsView(self.scene)
        self.view.setFixedSize(320, 200)
        
        # Ajouter le layout gauche et la vue graphique au layout principal
        main_layout.addLayout(left_layout)
        main_layout.addWidget(self.view)
        
        # Ajouter le point bleu au centre de la scène
        self.blue_point, self.blue_point_position = self.add_point(160, 100, "blue")
        
        # Rectangle à animer
        self.animated_rect = AnimatedRect(self.blue_point_position.x() - 5, self.blue_point_position.y() - 5, 10, 10)
        self.scene.addItem(self.animated_rect)
        
        # Objet animable
        self.animatable_item = AnimatableItem(self.animated_rect)
                 
        # Initialiser les variables pour les points et les lignes
        self.last_point = self.blue_point_position
        self.all_points = [self.blue_point_position]
        self.lines = []
        
        # Changer le curseur de la souris sur la vue
        self.view.setCursor(QCursor(Qt.CrossCursor))
        
        # Connecter l'événement de clic dans la scène
        self.view.mousePressEvent = self.on_scene_clicked

        # Centrer la vue sur le point bleu
        self.view.centerOn(self.blue_point)

    def add_point(self, x, y, color):
        radius = 5
        point = QGraphicsEllipseItem(x - radius, y - radius, radius * 2, radius * 2)
        point.setBrush(QColor(color))
        point.setPen(QPen(Qt.NoPen))
        self.scene.addItem(point)
        print("Added point at:", x, y)
        return point, QPointF(x, y)

    def add_line(self, point1_pos, point2_pos):
        line = QGraphicsLineItem(point1_pos.x(), point1_pos.y(), point2_pos.x(), point2_pos.y())
        line.setPen(QPen(QColor("black"), 2))
        self.scene.addItem(line)
        return line

    def on_scene_clicked(self, event):
        scene_pos = self.view.mapToScene(event.pos())
        print("Scene click position:", scene_pos.x(), scene_pos.y())
        item = self.scene.itemAt(scene_pos, self.view.transform())

        if item == self.blue_point or item in self.all_points:
            final_point, final_pos = self.add_point(scene_pos.x(), scene_pos.y(), "yellow")
            self.all_points.append(final_pos)
            print("Added yellow point at:", final_pos.x(), final_pos.y())
            new_line = self.add_line(self.last_point, final_pos)
            self.lines.append(new_line)
            self.last_point = final_pos            
        else:
            new_point, new_pos = self.add_point(scene_pos.x(), scene_pos.y(), "red")
            self.all_points.append(new_pos)
            print("Added red point at:", new_pos.x(), new_pos.y())
            new_line = self.add_line(self.last_point, new_pos)
            self.lines.append(new_line)
            self.last_point = new_pos
            
    def on_ok_clicked(self):
        blue_point_x = self.blue_point_position.x()
        blue_point_y = self.blue_point_position.y()

        all_points = [(point.x() - blue_point_x, point.y() - blue_point_y) for point in self.all_points]

        lines = [((line.line().x1() - blue_point_x, line.line().y1() - blue_point_y), 
                (line.line().x2() - blue_point_x, line.line().y2() - blue_point_y)) for line in self.lines]

        print("First Point: (0, 0)")
        print("All Points: ", all_points)
        print("Lines: ", lines)
        self.accept()

    def on_play_clicked(self):
        # Assurez-vous que le rectangle animé commence à la position du point bleu
        print("Position initiale du rectangle avant animation:", self.animatable_item.getPos())
        self.animatable_item.setPos(self.blue_point_position)
        print("Position après initialisation:", self.animatable_item.getPos())

        # Calculer les positions relatives
        blue_x, blue_y = self.blue_point_position.x(), self.blue_point_position.y()
        relative_points = [QPointF(point.x() - blue_x, point.y() - blue_y) for point in self.all_points]

        self.animation_group = QSequentialAnimationGroup()
        print("Nombre de points:", len(relative_points) - 1)
        for i in range(len(relative_points) - 1):
            start_pos = relative_points[i]
            end_pos = relative_points[i + 1]
            print(f"Animation de {start_pos} à {end_pos}")

            animation = QPropertyAnimation(self.animatable_item, b"pos")
            animation.setDuration(1000)
            animation.setStartValue(start_pos)
            animation.setEndValue(end_pos)
            animation.setEasingCurve(QEasingCurve.Linear)
            self.animation_group.addAnimation(animation)

        # Ajout d'un événement pour vérifier les positions après chaque animation
        def on_animation_finished():
            print("Position du rectangle après animation:", self.animatable_item.getPos())
            self.view.centerOn(self.animatable_item.getPos())  # Centrer la vue sur le rectangle animé

        self.animation_group.finished.connect(on_animation_finished)

        self.animation_group.start()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = PathWindow()
    window.show()
    sys.exit(app.exec_())
