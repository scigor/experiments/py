import sys, os, json

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QFileDialog, QMenuBar, QToolBar, QLabel, QPushButton, QDialog, QSlider, QSizePolicy, QSplitter, QScrollArea, QAbstractItemView, QCheckBox, QToolButton, QStackedWidget

from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTreeView, QTreeWidget, QTreeWidgetItem
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem

from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView, QGraphicsPixmapItem, QGraphicsTextItem, QGraphicsRectItem, QGraphicsEllipseItem
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QHBoxLayout, QFileSystemModel, QAction

from PyQt5.QtGui import QImage, QBrush, QPainter, QPen, QPixmap, QPolygonF, QColor, QDrag, QDragEnterEvent, QDropEvent, QFont
from PyQt5.QtGui import QColor, QPen, QCursor

from PyQt5.QtCore import Qt, QMimeData, QFileInfo, pyqtSignal, QPoint, QByteArray, QTimer, QLine, QRect, QRectF, QPointF, QLineF, QSizeF
from PyQt5.QtCore import QPropertyAnimation, QEasingCurve, QSequentialAnimationGroup, pyqtProperty, QObject

from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsLineItem, QGraphicsScene, QGraphicsView, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QApplication

configuration = {'map_width': 320, 'map_height': 224, 'grid_size': 16, 'exists': False}

class DraggableColorLabel(QLabel):
    def __init__(self, color):
        super().__init__()
        self.setFixedSize(30, 30)
        #self.color = color
        if isinstance(color, QColor):
            self.color = color
        else:
            self.color = QColor(color)        
        
        self.updateStyleSheet()
        self.setAcceptDrops(True)

    def updateStyleSheet(self):
        self.setStyleSheet(f"background-color: {self.color.name()}; border: 1px solid black;")

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_start_position = event.pos()

    def mouseMoveEvent(self, event):
        if not (event.buttons() & Qt.LeftButton):
            return
        if (event.pos() - self.drag_start_position).manhattanLength() < QApplication.startDragDistance():
            return

        drag = QDrag(self)
        mimedata = QMimeData()
        mimedata.setColorData(self.color)
        drag.setMimeData(mimedata)
        pixmap = QPixmap(self.size())
        self.render(pixmap)
        drag.setPixmap(pixmap)
        drag.setHotSpot(event.pos())
        drag.exec_(Qt.MoveAction)

    def dragEnterEvent(self, event):
        if event.mimeData().hasColor():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        new_color = event.mimeData().colorData()
        old_color = self.color
        self.color = new_color
        self.setStyleSheet(f"background-color: {QColor(new_color).name()}; border: 1px solid black;")
        event.source().color = old_color
        event.source().setStyleSheet(f"background-color: {QColor(old_color).name()}; border: 1px solid black;")

    def mouseDoubleClickEvent(self, event):
        self.openColorAdjuster()

    def openColorAdjuster(self):
        self.color_window = ColorAdjustWindow(self.color, self)
        self.color_window.show()

    def updateColor(self, new_color):
        if isinstance(new_color, QColor):
            self.color = new_color
        else:
            self.color = QColor(new_color)
        self.updateStyleSheet()

class ColorAdjustWindow(QWidget):
    def __init__(self, color, parent=None):
        super().__init__()
        self.color = color
        self.initUI()
        self.parent_label = parent
        
    def initUI(self):
        main_layout = QVBoxLayout(self)

        self.color_preview = QLabel()
        self.color_preview.setStyleSheet("background-color: " + self.color.name())
        self.color_preview.setFixedHeight(30)  # Fixe seulement la hauteur
        self.color_preview.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        main_layout.addWidget(self.color_preview)
        
        # Disposition des sliders et des labels hexadécimaux
        sliders_layout = QHBoxLayout()

        self.sliders = []
        self.hex_labels = []
        labels = ['Red', 'Green', 'Blue']
        for i, comp in enumerate([self.color.red(), self.color.green(), self.color.blue()]):
            # Conteneur vertical pour chaque slider et son label
            slider_container = QVBoxLayout()

            slider = QSlider(Qt.Vertical)
            slider.setMaximum(255)
            slider.setValue(comp)
            slider.valueChanged.connect(lambda value, idx=i, s=self: s.updateColor(idx, value))

            label = QLabel(f"{labels[i]}: {comp:02X}")
            label.setAlignment(Qt.AlignCenter)  # Centre le texte sous le slider

            slider_container.addWidget(slider)
            slider_container.addWidget(label)

            sliders_layout.addLayout(slider_container)
            self.sliders.append(slider)
            self.hex_labels.append(label)

        main_layout.addLayout(sliders_layout)

        btn_ok = QPushButton('OK')
        btn_ok.clicked.connect(self.applyChanges)
        main_layout.addWidget(btn_ok)

    def updateColor(self, index, value):
        # Mise à jour de la couleur
        if index == 0:
            self.color.setRed(value)
        elif index == 1:
            self.color.setGreen(value)
        elif index == 2:
            self.color.setBlue(value)

        # Mise à jour de l'aperçu de couleur et du label hexadécimal
        self.color_preview.setStyleSheet("background-color: " + self.color.name())
        self.hex_labels[index].setText(f"{['Red', 'Green', 'Blue'][index]}: {value:02X}")

    def applyChanges(self):
        if self.parent_label:
            self.parent_label.updateColor(self.color)
        self.close()

class ColorPaletteWindow(QWidget):
    def __init__(self, image, scene):#color_table, pixmap_item):
        super().__init__()
        self.image = image
        self.scene = scene        
        self.initUI()
        
    def initUI(self):
        # Création de la zone défilable
        scroll_area = QScrollArea(self)
        scroll_area.setWidgetResizable(True)
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        # Widget interne pour contenir la grille
        container = QWidget()
        scroll_area.setWidget(container)
        layout = QGridLayout(container)
        layout.setSpacing(5) 
        
        self.labels = []
        color_table = self.image.colorTable()
        cell_size = 30
        cells_per_row = 16
        
        for i, color in enumerate(self.image.colorTable()):
            label = DraggableColorLabel(color)
            self.labels.append(label)
            layout.addWidget(label, i // cells_per_row, i % cells_per_row)

        # Définir la taille minimum du container pour s'assurer qu'il s'étend correctement
        num_rows = (len(color_table) + cells_per_row - 1) // cells_per_row
        container.setMinimumSize(cells_per_row * (cell_size + 5), num_rows * (cell_size + 5))

        # Définir la taille préférée de la zone de défilement
        scroll_area.setMinimumSize(cells_per_row * (cell_size + 5), 4 * (cell_size + 5))

        # Layout principal du widget ColorPaletteWindow
        main_layout = QVBoxLayout(self)
        main_layout.addWidget(scroll_area)

        # Ajouter le bouton OK en dehors de la zone de défilement
        btn_ok = QPushButton('OK', self)
        btn_ok.clicked.connect(self.updateImage)
        main_layout.addWidget(btn_ok)

        self.setWindowTitle('Palette de couleurs')
        self.setLayout(main_layout)

    def updateImage(self):
        for i, label in enumerate(self.labels):
            self.image.setColor(i, QColor(label.color).rgb())
        self.image = self.image.convertToFormat(QImage.Format_Indexed8)
        pixmap = QPixmap.fromImage(self.image)
        self.scene.addPixmap(pixmap)
        self.close()

class TreeManager:
    def __init__(self, tree_widget):
        self.tree = tree_widget

    def add_layer(self, name, properties):
        new_item = QTreeWidgetItem()
        new_item.setText(0, name)
        new_item.setData(0, Qt.UserRole, properties)
        self.tree.addTopLevelItem(new_item)
        return new_item
        
    def remove_layer(self):
        item = self.tree.currentItem()
        if item:
            index = self.tree.indexOfTopLevelItem(item)
            self.tree.takeTopLevelItem(index)

    def move_layer_up(self):
        current_index = self.tree.currentIndex().row()
        if current_index > 0:
            item = self.tree.takeTopLevelItem(current_index)
            self.tree.insertTopLevelItem(current_index - 1, item)
            self.tree.setCurrentItem(item)

    def move_layer_down(self):
        current_index = self.tree.currentIndex().row()
        if current_index < self.tree.topLevelItemCount() - 1:
            item = self.tree.takeTopLevelItem(current_index)
            self.tree.insertTopLevelItem(current_index + 1, item)
            self.tree.setCurrentItem(item)

class TileWidgetManager(QWidget):

    tileSelectionChanged = pyqtSignal(list)

    def __init__(self, parent=None):
        super().__init__(parent)
        
        self.tile_widget = None
        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)
        
    def create_tile_widget(self, image_path, tile_size):
    
        if self.tile_widget:
            self.layout.removeWidget(self.tile_widget)
            self.tile_widget.deleteLater()

        self.tile_widget = TileWidget(image_path, tile_size)
        self.layout.addWidget(self.tile_widget)

        # Connecter le signal de la scène à celui du manager
        self.tile_widget.scene.selectionChanged.connect(self.tileSelectionChanged)
        
    def remove_tile_widget(self):
        if self.tile_widget is not None:
            self.tile_widget.deleteLater()  # Supprime le widget et nettoie les ressources
            self.tile_widget = None  # Réinitialise la référence à None
            
    def on_tile_selection_changed(self, selected_tiles):
        self.tileSelectionChanged.emit(selected_tiles)

             
class TileWidget(QGraphicsView):

    def __init__(self, image_path, tile_size, parent=None):
        super().__init__(parent)
        self.zoom_level = 1
        self.initial_zoom_level = 1
        self.scene = TileScene(image_path, tile_size, 2, self)
        self.setScene(self.scene)
        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
              
    def wheelEvent(self, event):
        old_zoom_level = self.zoom_level
        if event.angleDelta().y() > 0:
            self.zoom_level = min(self.zoom_level + 0.1, 3)
        else:
            self.zoom_level = max(self.zoom_level - 0.1, self.initial_zoom_level)

        if old_zoom_level != self.zoom_level:
            self.scene.update_tiles(self.zoom_level)  # Mise à jour des tuiles dans la scène
        event.accept()

class TileScene(QGraphicsScene):

    # Signal pour envoyer les tuiles sélectionnées
    selectionChanged = pyqtSignal(list)
    
    def __init__(self, image_path, tile_size, cell_space=2, parent=None):
        super().__init__(parent)
        self.tile_size = tile_size
        self.cell_space = cell_space                
        self.pixmap = QPixmap(image_path)
        self.tiles = []
        self.init_tiles()
        self.selectionChanged.connect(self.print_selected_tiles)

    def init_tiles(self):
        rows = self.pixmap.height() // self.tile_size
        cols = self.pixmap.width() // self.tile_size

        for row in range(rows):
            row_tiles = []  # Une nouvelle liste pour chaque ligne
            for col in range(cols):
                cropped_pixmap = self.pixmap.copy(col * self.tile_size, row * self.tile_size, self.tile_size, self.tile_size)
                item = QGraphicsPixmapItem(cropped_pixmap)
                item.setPos(col * (self.tile_size + self.cell_space), row * (self.tile_size + self.cell_space))
                self.addItem(item)
                row_tiles.append(item)
            self.tiles.append(row_tiles)  # Ajouter la ligne à la liste des tuiles

        self.selection_rect = QGraphicsRectItem()
        self.selection_rect.setPen(Qt.red)
        self.selection_rect.setBrush(Qt.transparent)
        self.addItem(self.selection_rect)

    def update_tiles(self, zoom_level):
        for row in range(len(self.tiles)):
            for col in range(len(self.tiles[row])):
                item = self.tiles[row][col]
                # Mettre à jour la position et la taille
                new_size = self.tile_size * zoom_level
                item.setPixmap(self.pixmap.copy(col * self.tile_size, row * self.tile_size, self.tile_size, self.tile_size).scaled(new_size, new_size))
                item.setPos(col * (new_size + self.cell_space), row * (new_size + self.cell_space))

        # Mettre à jour la taille de la scène si nécessaire
        self.setSceneRect(self.itemsBoundingRect())  # Ajuste la taille de la scène aux éléments qu'elle contient
        
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            # Cacher le rectangle de sélection précédent et initialiser un nouveau
            if self.selection_rect.isVisible():
                self.selection_rect.hide()  # Cache l'ancien rectangle de sélection
            self.origin = event.scenePos()
            self.selection_rect.setRect(QRectF(self.origin, QSizeF()))
            self.selection_rect.show()
        super().mousePressEvent(event)

    def align_to_grid(self, pos):
        x = (pos.x() // (self.tile_size + self.cell_space)) * (self.tile_size + self.cell_space)
        y = (pos.y() // (self.tile_size + self.cell_space)) * (self.tile_size + self.cell_space)
        return QPointF(x, y)
        
    def mouseMoveEvent(self, event):
        if self.origin and event.buttons() == Qt.LeftButton:
            current_pos = self.align_to_grid(event.scenePos())
            rect = QRectF(self.origin, current_pos).normalized()
            self.selection_rect.setRect(rect)
        super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton and self.origin:
            self.origin = None
            selected_tiles = []
            rect = self.selection_rect.rect()
            if not rect.isEmpty():
                for row_tiles in self.tiles:
                    for item in row_tiles:
                        if rect.intersects(item.sceneBoundingRect()):
                            selected_tiles.append(item)
            self.selectionChanged.emit(selected_tiles)
        super().mouseReleaseEvent(event)
        
    def print_selected_tiles(self, selected_tiles):
        for item in selected_tiles:
            x = item.x() // (self.tile_size + self.cell_space)
            y = item.y() // (self.tile_size + self.cell_space)
            print(f"Tile at position: ({x}, {y})")          
    
class CustomTreeWidget(QTreeWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setDragEnabled(True)
        self.setDragDropMode(QAbstractItemView.DragOnly)

    def startDrag(self, supportedActions):
        item = self.currentItem()
        if item:
            properties = item.data(0, Qt.UserRole)
            if properties:
                mimeData = QMimeData()
                # Sérialiser les propriétés en JSON et les mettre dans les données mime
                serialized_properties = json.dumps(properties)
                mimeData.setText(serialized_properties)

                drag = QDrag(self)
                drag.setMimeData(mimeData)
                
                ## Définir une pixmap pour l'aperçu du drag (facultatif)
                #pixmap = QPixmap(100, 100)
                #pixmap.fill(Qt.white)
                #painter = QPainter(pixmap)
                #painter.drawText(pixmap.rect(), Qt.AlignCenter, "Dragging...")
                #painter.end()

                #drag.setPixmap(pixmap)
                drag.exec_(Qt.MoveAction)
                
class MyGraphicsView(QGraphicsView):

    spriteAdded = pyqtSignal(dict)
    spriteDeleted = pyqtSignal(dict)
    spriteUpdated = pyqtSignal(dict)
            
    def __init__(self, scene, parent=None):
    
        super().__init__(scene, parent)
                
        # Définit une taille minimale pour la vue (par exemple 320x240)
        self.setMinimumSize(configuration['map_width'], configuration['map_height'])

        # Politique de redimensionnement : extensible dans les deux directions
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        
        self.setAcceptDrops(True)
        self.setScene(scene)

        # Configuration de la vue pour gérer les barres de défilement
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        
        # Activer le zoom
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)

        # Paramètres de la grille
        self.gridSize = configuration['grid_size']
        self.visibleGridCellsX = configuration['map_width'] // self.gridSize  
        self.visibleGridCellsY = configuration['map_height'] // self.gridSize
        self.showGrid = True

        # Configuration de la vue pour s'assurer que la grille est visible au démarrage
        self.scale(1.0, 1.0)  # Ajustez le facteur de zoom initial si nécessaire

        # Configuration du label de la scène        
        font = QFont("Arial", 24)
        font.setBold(True)
        
        self.scene_label = QGraphicsTextItem("")
        self.scene_label.setPos(100, 100)        
        self.scene_label.setFont(font)
        self.scene().addItem(self.scene_label)
        self.scene_label.hide()

        # Timer to control label visibility
        self.scene_label_timer = QTimer(self)
        self.scene_label_timer.setInterval(2000)  # 2 seconds
        self.scene_label_timer.setSingleShot(True)
        self.scene_label_timer.timeout.connect(self.hide_scene_label)

        #self.updateSceneLayer({'id': 0, 'grid_size': self.gridSize, 'visible': 'true'})

        self.item_id_counter = 0
        self.adjustSceneSize() 

        #self.actived_sprites = True
        #self.actived_tiles = True
        self.active_button_id = None

        self.selected_tiles = []

        self.sprite_item = None
        self.drag_start_pos = None

    def toggleSceneGrid(self, visible):
    
        self.showGrid = visible
        self.viewport().update()
    
    def toggleMouseEvent(self, active_button_id):
        self.active_button_id = active_button_id
        print(f"{active_button_id}")

    def drawForeground(self, painter, rect):
        if configuration['exists'] == False:
            return
        if self.showGrid:
            # Obtenir la transformation actuelle pour comprendre le niveau de zoom
            scale = self.transform().m11()

            # Calculer la taille réelle de la grille ajustée au zoom
            realGridSize = self.gridSize * scale

            # Centre de la scène
            sceneCenter = self.sceneRect().center()

            # Calculer les points de départ pour dessiner les lignes
            # Ajuster startX et startY pour qu'ils soient centrés autour du centre de la scène
            startX = sceneCenter.x() - (self.visibleGridCellsX / 2 * realGridSize)
            startY = sceneCenter.y() - (self.visibleGridCellsY / 2 * realGridSize)

            lines = []
            # Dessiner les lignes verticales
            for i in range(self.visibleGridCellsX + 1):  # +1 pour fermer la dernière cellule
                x = startX + i * realGridSize
                lines.append(QLineF(x, startY, x, startY + self.visibleGridCellsY * realGridSize))

            # Dessiner les lignes horizontales
            for j in range(self.visibleGridCellsY + 1):
                y = startY + j * realGridSize
                lines.append(QLineF(startX, y, startX + self.visibleGridCellsX * realGridSize, y))

            # Définir la couleur et le style de la grille
            painter.setPen(QPen(Qt.lightGray, 1, Qt.SolidLine))
            painter.drawLines(lines)

    def wheelEvent(self, event):
    
        factor = 1.25 if event.angleDelta().y() > 0 else 0.8
        newScale = self.transform().m11() * factor
        
        if newScale < 1.0:
            factor = 1.0 / self.transform().m11()
        elif newScale > 3.05:  # Limite maximale basée sur vos données
            factor = 3.05 / self.transform().m11()

        self.scale(factor, factor)
        self.adjustSceneSize()
        self.centerVerticalScrollBar()
        self.update()

    def adjustSceneSize(self):
    
        factor = self.transform().m11()  # Récupération du facteur de zoom actuel
        print(f"Zoom factor: {factor}")  # Imprime le facteur de zoom actuel
        
        newWidth = self.gridSize * self.visibleGridCellsX * factor
        newHeight = self.gridSize * self.visibleGridCellsY * factor

        # Adapter les paddings dynamiquement avec le zoom
        padding = max(50, 50 * factor)  # Un maximum ou une formule ajustée pour limiter le padding
        newRect = QRectF(-padding, -padding, newWidth + 2 * padding, newHeight + 2 * padding)
        self.setSceneRect(newRect)
        
        self.adjustImagePositions()
        self.update()
        print(f"Scene rect: {newRect}")

    def centerVerticalScrollBar(self):
    
        vScroll = self.verticalScrollBar()
        vScroll.setValue((vScroll.maximum() - vScroll.minimum()) // 2)

    def adjustImagePositions(self):
    
        scale = self.transform().m11()  # Facteur de zoom actuel
        for item in self.scene().items():
            if isinstance(item, QGraphicsPixmapItem):
                # Récupère la position originale
                initial_pos = item.data(0)
                if initial_pos is not None:
                    # Mettre à jour la position
                    new_x = initial_pos.x() * scale
                    new_y = initial_pos.y() * scale
                    item.setPos(new_x, new_y)
                    
                    # Adapter la taille de l'image au zoom
                    original_size = item.data(1)
                    if original_size:
                        item.setScale(scale)

    def dragEnterEvent(self, event):
    
        #if event.mimeData().hasFormat("application/json"):
        if event.mimeData().hasText():
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
    
        #if event.mimeData().hasFormat("application/json"):
        if event.mimeData().hasText():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
    
        mimeData = event.mimeData()
        if mimeData.hasText():
        
            serialized_properties = mimeData.text()
            print("Received data:", serialized_properties)
            if serialized_properties:
                properties = json.loads(serialized_properties)
                self.addImageToScene(properties, event.pos())
                event.accept()
            else:
                print("No data received or data is empty.")
                event.ignore()            
        else:
            event.ignore()        

    def hide_scene_label(self):
    
        self.scene_label.hide()
        
    def addImageToScene(self, properties, position):
    
        pixmap = QPixmap(properties['File Path'])
        if not pixmap.isNull():
            # Convertir la position en coordonnées de la scène tenant compte du zoom
            scale = self.transform().m11()
            scenePosition = self.mapToScene(position)
            adjustedPosition = QPointF(scenePosition.x() / scale, scenePosition.y() / scale)

            gridWidth = self.gridSize * self.visibleGridCellsX
            gridHeight = self.gridSize * self.visibleGridCellsY
            gridRect = QRectF(0, 0, gridWidth, gridHeight)

            if gridRect.contains(adjustedPosition):
                item = QGraphicsPixmapItem(pixmap)
                item.setPos(adjustedPosition)
                item.setData(0, adjustedPosition)
                item.setData(1, pixmap.size())

                self.item_id_counter += 1
                item_properties = { 'id':  self.item_id_counter, 'layer': self.scene_layer_id, 'File Path': properties['File Path'], 'visible':  self.scene_visible}
                item.setData(2, item_properties)

                self.scene().addItem(item)                
                self.spriteAdded.emit(item_properties)

            else:
                print("Image position is outside the grid area.")
                
    def updateSceneLayer(self, properties):
        
        print(f"Layer {properties}")
        self.scene_layer_id = properties['id']       
        self.scene_visible = properties['visible']
        self.scene_label.setPlainText(f"Layer {properties['id']}")
                
        self.gridSize = properties['grid_size']
        self.visibleGridCellsX = configuration['map_width'] // self.gridSize  
        self.visibleGridCellsY = configuration['map_height'] // self.gridSize
  
        self.scene_label.show()
        self.scene_label_timer.start()
        
        self.updateImageToScene(properties)
        self.viewport().update()

    def updateImageToScene(self, properties):
        for item in self.scene().items():
            if isinstance(item, QGraphicsPixmapItem):
                # Récupère les properties originale
                item_properties = item.data(2)
                if item_properties is not None:       
                    if item_properties['layer'] == self.scene_layer_id:
                        #print(f"Image {item_properties} {self.scene_layer_id}")
                    
                        item_properties['visible'] = properties['visible']
                        item.setData(2, item_properties)
                        item.setVisible( item_properties['visible'].lower() in ['true', '1', 't', 'y', 'yes'] )

                        self.spriteUpdated.emit(properties) # a garder ??
                        
    def deleteSceneLayer(self, properties):
        layer_id = properties['id']
        #print(f"Layer {properties}")
        items_to_remove = []
        for item in self.scene().items():
            if isinstance(item, QGraphicsPixmapItem):
                # Récupère les properties originale
                item_properties = item.data(2)
                if item_properties and item_properties.get('layer') == layer_id:
                    items_to_remove.append(item)
                    self.spriteDeleted.emit(item_properties)

        for item in items_to_remove:
            self.scene().removeItem(item)

    def deleteSceneSprite(self, properties):
        sprite_id = properties['id']
        #print(f"Sprite {properties}")
        items_to_remove = []
        for item in self.scene().items():
            if isinstance(item, QGraphicsPixmapItem):
                # Récupère les properties originale
                item_properties = item.data(2)
                #print(f"Image {item_properties}")
                if item_properties and item_properties.get('id') == sprite_id:
                    items_to_remove.append(item)
        
        for item in items_to_remove:
            self.scene().removeItem(item)
            
    def setSelectedTiles(self, tiles):
        self.selected_tiles = tiles        
        print("Selected tiles set in MyGraphicsView:", tiles)
        
    def mousePressEvent(self, event):
        if self.active_button_id == 1 and event.button() == Qt.LeftButton:
            pos = event.pos()
            scene_pos = self.mapToScene(pos)
            self.drawTiles(scene_pos)
        if self.active_button_id == 2:
            # Vérifiez si l'utilisateur clique sur un item
            item = self.itemAt(event.pos())
            if isinstance(item, QGraphicsPixmapItem):
                self.sprite_item = item
                self.drag_start_pos = event.pos()
                self.setCursor(Qt.ClosedHandCursor)  # Changez le curseur pour indiquer un drag
            else:
                super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if self.sprite_item:
            # Calculez la nouvelle position de l'item en fonction du mouvement de la souris
            delta = self.mapToScene(event.pos()) - self.mapToScene(self.drag_start_pos)
            new_pos = self.sprite_item.pos() + delta
            self.sprite_item.setPos(new_pos)
            self.drag_start_pos = event.pos()  # Réinitialiser la position de départ pour le prochain mouvement
        else:
            super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        if self.sprite_item:
            self.setCursor(Qt.ArrowCursor)  # Réinitialisez le curseur
            self.sprite_item = None  # Réinitialisez l'item en cours de déplacement
            self.drag_start_pos = None  # Réinitialisez la position de départ
        else:
            super().mouseReleaseEvent(event)

    def calculate_grid_dimensions(self, selected_tiles):
        if not selected_tiles:
            return 0, 0
        
        min_x = min(tile.x() for tile in selected_tiles)
        max_x = max(tile.x() for tile in selected_tiles)
        min_y = min(tile.y() for tile in selected_tiles)
        max_y = max(tile.y() for tile in selected_tiles)

        gridWidth = ((max_x - min_x) // self.gridSize) + 1
        gridHeight = ((max_y - min_y) // self.gridSize) + 1
        
        return gridWidth, gridHeight

    def drawTiles(self, pos):
        if not self.selected_tiles:
            return

        gridX = int(pos.x() // self.gridSize)
        gridY = int(pos.y() // self.gridSize)

        gridWidth, gridHeight = self.calculate_grid_dimensions(self.selected_tiles)
        for idx, tile in enumerate(self.selected_tiles):
            x = (gridX + (idx % gridWidth)) * self.gridSize
            y = (gridY + (idx // gridWidth)) * self.gridSize
            item = QGraphicsPixmapItem(tile.pixmap())
            item.setPos(x, y)
            self.scene().addItem(item)
            item.setData(0, QPointF(x, y))
            item.setData(1, tile.pixmap().size()) 
            item_properties = { 'id':  y*self.gridSize+x, 'layer': self.scene_layer_id, 'visible':  self.scene_visible}
            item.setData(2, item_properties)            

class LayerTab(QWidget):

    def __init__(self, tab_name="Layers"):
    
        super().__init__()
    
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.item_id_counter = 0  

        self.toolbar = QToolBar()
        layout.addWidget(self.toolbar)

        self.tree = CustomTreeWidget()
        self.tree.setHeaderLabel(tab_name)
        layout.addWidget(self.tree)
        self.tree_manager = TreeManager(self.tree)

        self.setup_actions()

    def setup_actions(self):
        self.toolbar.addAction("+", self.add_layer_dialog)
        self.toolbar.addAction("-", self.remove_layer_dialog)
        self.toolbar.addAction("↑", self.tree_manager.move_layer_up)
        self.toolbar.addAction("↓", self.tree_manager.move_layer_down)

    def add_layer_dialog(self):
        # Default behavior, can be overridden in subclasses
        self.tree_manager.add_layer("Layer " + str(self.item_id_counter))
        self.item_id_counter += 1

    def remove_layer_dialog(self):
        # Default behavior, can be overridden in subclasses
        self.tree_manager.remove_layer()

    def resetTab(self):
        self.tree.clear()
        self.item_id_counter = 0

class TilesetTabWithFileDialog(LayerTab):

    def __init__(self, tab_name="Tilesets"):
    
        super().__init__(tab_name)

        # Ajout d'un QSplitter pour séparer l'arbre et la table de propriétés
        self.splitter = QSplitter(Qt.Vertical)
        self.layout().addWidget(self.splitter)

        self.splitter.addWidget(self.tree)

        self.property_table = QTableWidget(0, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.splitter.addWidget(self.property_table)

        # Utiliser le ScrollArea du TileWidgetManager
        self.tile_widget_manager = TileWidgetManager(self)
        self.layout().addWidget(self.tile_widget_manager)
     
        self.tree.currentItemChanged.connect(self.update_properties)
        
        self.tile_widget_manager.tileSelectionChanged.connect(self.on_tile_selection_changed)

    #def on_tile_selection_changed(self, selected_tiles):
    #    if self.view:
    #        self.view.setSelectedTiles(selected_tiles)
    def on_tile_selection_changed(self, selected_tiles):
        parent_widget = self.parent()
        while parent_widget and not hasattr(parent_widget, 'view'):
            parent_widget = parent_widget.parent()
    
        if parent_widget and hasattr(parent_widget, 'view'):
            parent_widget.view.setSelectedTiles(selected_tiles)      

    def add_layer_dialog(self):
        if configuration['exists'] == False:
            return
        dialog = QDialog(self)
        dialog.setWindowTitle("Add Tileset Layer")
        layout = QVBoxLayout()

        file_label = QLabel("No file selected")
        file_btn = QPushButton("Select File")
        file_btn.clicked.connect(lambda: self.open_file_dialog(file_label))

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(16)
        self.slider.setMaximum(64)
        self.slider.setSingleStep(16)
        self.slider.setTickInterval(16)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setValue(16)
        
        self.slider_label = QLabel("16", self)
        self.slider_label.setAlignment(Qt.AlignCenter)
        
        self.position_label()
        
        #slider.valueChanged.connect(lambda value: slider_label.setText(str(value)))
        self.slider.valueChanged.connect(self.update_slider_label)

        ok_btn = QPushButton("OK")
        ok_btn.clicked.connect(lambda: self.accept_tileset(file_label.text(), self.slider.value(), dialog))

        layout.addWidget(file_label)
        layout.addWidget(file_btn)
        layout.addWidget(self.slider)
        layout.addWidget(self.slider_label)
        layout.addWidget(ok_btn)

        dialog.setLayout(layout)
        dialog.exec_()

    def remove_layer_dialog(self):
        self.tile_widget_manager.remove_tile_widget()
        self.tree_manager.remove_layer() 

    def update_properties(self, current, previous):
                
        self.property_table.clearContents()
        self.property_table.setRowCount(0)
        if current:
            properties = current.data(0, Qt.UserRole)
            if properties:
                file_path = properties.get("File Path")
                tile_size = properties.get("Tile Size")
                if file_path and tile_size:
                    self.tile_widget_manager.create_tile_widget(file_path, tile_size)

                self.property_table.setRowCount(len(properties))
                for i, (key, value) in enumerate(properties.items()):
                    self.property_table.setItem(i, 0, QTableWidgetItem(key))
                    self.property_table.setItem(i, 1, QTableWidgetItem(str(value)))

    def accept_tileset(self, file_path, tile_size, dialog):
        if file_path and not file_path.startswith("No file selected"):
            item_name = file_path.split('/')[-1]
            properties = {'File Path': file_path, 'Tile Size': tile_size}
            self.tree_manager.add_layer(item_name, properties)
            self.update_properties(self.tree.currentItem(), None)
            dialog.accept()
        else:
            QMessageBox.warning(self, "Error", "No file selected or invalid file.")

    def open_file_dialog(self, label):
        file_path, _ = QFileDialog.getOpenFileName(self, "Select Tileset Image", "", "Image Files (*.png *.jpg *.bmp)")
        if file_path:
            label.setText(file_path)
            
    def update_slider_label(self, value):
        # Adjust slider value to the nearest multiple of 16
        adjusted_value = round((value - 16) / 16) * 16 + 16
        self.slider.setValue(adjusted_value)  # Set the slider to the adjusted value
        self.slider_label.setText(str(adjusted_value))
        self.position_label()

    def position_label(self):
        # Get the current value and maximum value from the slider
        value = self.slider.value()
        maximum = self.slider.maximum()

        # Calculate handle position
        handle_position = (value - self.slider.minimum()) / (self.slider.maximum() - self.slider.minimum())

        # Get slider geometry
        slider_rect = self.slider.geometry()

        # Calculate label position based on handle position
        label_x = slider_rect.x() + handle_position * slider_rect.width() - self.slider_label.width() / 2
        label_y = slider_rect.y() + slider_rect.height() + 5  # 5 pixels below the slider

        # Set the label position
        self.slider_label.move(int(label_x), label_y)

    def resetTab(self):
        super().resetTab()
        self.property_table.clearContents()
        self.tile_widget_manager.remove_tile_widget()
        
class LayerTabStandard(LayerTab):

    LayerUpdatedProps  = pyqtSignal(dict)

    LayerDeleted = pyqtSignal(dict)

    def __init__(self, tab_name="Layers", scene=None, view=None):
    
        super().__init__(tab_name)
        
        self.scene = scene

        self.splitter = QSplitter(Qt.Vertical)
        self.layout().addWidget(self.splitter)
        self.splitter.addWidget(self.tree)

        self.property_table = QTableWidget(2, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.splitter.addWidget(self.property_table)
        
        self.tree.currentItemChanged.connect(self.update_properties)        

    def add_layer_dialog_configuration(self):
        if configuration['exists'] == False:
            return
        # Default behavior, can be overridden in subclasses
        properties = {'id': self.item_id_counter, 'grid_size': configuration['grid_size'], 'visible': 'true'}
        layer_name = "Layer " + str(self.item_id_counter)
        new_item = self.tree_manager.add_layer(layer_name, properties)
        self.tree.setCurrentItem(new_item)
        self.update_properties(new_item, None)

        self.item_id_counter += 1

    def add_layer_dialog(self):
        if configuration['exists'] == False:
            return
        dialog = QDialog(self)
        dialog.setWindowTitle("Add Layer")
        
        layout = QVBoxLayout()

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(16)
        self.slider.setMaximum(64)
        self.slider.setSingleStep(16)
        self.slider.setTickInterval(16)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setValue(16)
        
        self.slider_label = QLabel("16", self)
        self.slider_label.setAlignment(Qt.AlignCenter)
        
        self.position_label()
        
        self.slider.valueChanged.connect(self.update_slider_label)

        ok_btn = QPushButton("OK")
        ok_btn.clicked.connect(lambda: self.accept_layer(self.slider.value(), dialog))

        layout.addWidget(self.slider)
        layout.addWidget(self.slider_label)
        layout.addWidget(ok_btn)

        dialog.setLayout(layout)
        dialog.exec_()
        
    def accept_layer(self, grid_size, dialog):
        properties = {'id': self.item_id_counter, 'grid_size': grid_size, 'visible': 'true'}
        layer_name = "Layer " + str(self.item_id_counter)
        new_item = self.tree_manager.add_layer(layer_name, properties)
        self.tree.setCurrentItem(new_item)
        self.update_properties(new_item, None)
        self.item_id_counter += 1
        dialog.accept()

    def update_slider_label(self, value):
        # Adjust slider value to the nearest multiple of 16
        adjusted_value = round((value - 16) / 16) * 16 + 16
        self.slider.setValue(adjusted_value)  # Set the slider to the adjusted value
        self.slider_label.setText(str(adjusted_value))
        self.position_label()

    def position_label(self):
        # Get the current value and maximum value from the slider
        value = self.slider.value()
        maximum = self.slider.maximum()

        # Calculate handle position
        handle_position = (value - self.slider.minimum()) / (self.slider.maximum() - self.slider.minimum())

        # Get slider geometry
        slider_rect = self.slider.geometry()

        # Calculate label position based on handle position
        label_x = slider_rect.x() + handle_position * slider_rect.width() - self.slider_label.width() / 2
        label_y = slider_rect.y() + slider_rect.height() + 5  # 5 pixels below the slider

        # Set the label position
        self.slider_label.move(int(label_x), label_y)
        
    def remove_layer_dialog(self):
        if self.tree.topLevelItemCount() > 1:
            item = self.tree.currentItem()
            if item:
                properties = item.data(0, Qt.UserRole)
                self.LayerDeleted.emit(properties)
                self.tree_manager.remove_layer()            
        else:
            pass
            #QMessageBox.information(self, "Remove Layer", "Cannot remove the last remaining layer.")

    def update_properties(self, current, previous):
        self.property_table.clearContents()
        self.property_table.setRowCount(0)
        if current:
            properties = current.data(0, Qt.UserRole)
            if properties:
                row_count = len(properties)
                self.property_table.setRowCount(row_count)
                for id, (key, value) in enumerate(properties.items()):
                    key_item = QTableWidgetItem(key)
                    self.property_table.setItem(id, 0, key_item)
                    
                    if key == 'visible':
                        checkbox = QCheckBox()
                        is_checked = value.lower() in ['true', '1', 't', 'y', 'yes']
                        checkbox.setChecked(is_checked)
                        checkbox.stateChanged.connect(lambda state, item=current: self.checkbox_changed(item, key, state))
                        self.property_table.setCellWidget(id, 1, checkbox)
                    else:
                        value_item = QTableWidgetItem(str(value))
                        self.property_table.setItem(id, 1, value_item)

            self.LayerUpdatedProps.emit(properties)

    def checkbox_changed(self, item, key, state):
        new_value = 'true' if state == Qt.Checked else 'false'
        properties = item.data(0, Qt.UserRole)
        properties[key] = new_value
        item.setData(0, Qt.UserRole, properties)        
        self.LayerUpdatedProps.emit(properties)

    def update_scene_properties(self, properties):
        print(f"Emission du signal LayerUpdatedProps avec le texte : {properties}")
        self.LayerUpdatedProps.emit(properties)

    def resetTab(self):
        super().resetTab()
        self.property_table.clearContents()
                           
class SpriteTemplateTab(LayerTab):

    def __init__(self, tab_name="Template", scene=None, view=None):
        super().__init__(tab_name)

        self.scene = scene
        self.view = view
        
        self.splitter = QSplitter(Qt.Vertical)
        self.layout().addWidget(self.splitter)
        self.splitter.addWidget(self.tree)

        self.property_table = QTableWidget(2, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.splitter.addWidget(self.property_table)

        self.tree.currentItemChanged.connect(self.update_properties)

    def add_layer_dialog(self):
        if configuration['exists'] == False:
            return
        file_name, _ = QFileDialog.getOpenFileName(self, "Open File")
        if file_name:
            name = file_name.split('/')[-1]  # Extract file name*
            properties = {'File Path': file_name}
            new_item = self.tree_manager.add_layer(name, properties)
            self.tree.setCurrentItem(new_item)
            self.update_properties(self.tree.currentItem(), None)
            
            # Set additional data
            for key, value in properties.items():
                new_item.setData(0, Qt.UserRole, properties)  # You can also store the whole dictionary            

    def update_properties(self, current, previous):
        self.property_table.clearContents()
        self.property_table.setRowCount(0)
        if current:
            properties = current.data(0, Qt.UserRole)
            if properties:
                row_count = len(properties)
                self.property_table.setRowCount(row_count)
                for index, (key, value) in enumerate(properties.items()):
                    self.property_table.setItem(index, 0, QTableWidgetItem(key))
                    self.property_table.setItem(index, 1, QTableWidgetItem(str(value)))

    def resetTab(self):
        super().resetTab()
        self.property_table.clearContents()
               
class SpriteTab(LayerTab):

    spriteDeleted = pyqtSignal(dict)

    def setup_actions(self):

        super().setup_actions()
        
        self.disable_add_action()

        self.splitter = QSplitter(Qt.Vertical)
        self.layout().addWidget(self.splitter)
        self.splitter.addWidget(self.tree)

        self.property_table = QTableWidget(2, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.splitter.addWidget(self.property_table)

        self.tree.currentItemChanged.connect(self.update_properties)
    
    def disable_add_action(self):
        addAction = self.toolbar.findChild(QAction, "addAction")
        if addAction:
            # Désactiver le bouton "+"
            addAction.setDisabled(True)

    def add_layer_dialog(self):
        # Redéfinir pour ne rien faire
        pass
    
    def remove_layer_dialog(self):
        item = self.tree.currentItem()
        if item:
            properties = item.data(0, Qt.UserRole)
            self.spriteDeleted.emit(properties)
            super().remove_layer_dialog()
        
    def update_properties(self, current, previous):
        self.property_table.clearContents()
        self.property_table.setRowCount(0)
        if current:
            properties = current.data(0, Qt.UserRole)
            if properties:
                self.add_or_update_properties(properties)
    
    def handle_new_sprite_properties(self, properties):
        new_item = self.tree_manager.add_layer("Sprite" + str(properties['id']), properties)
        #self.tree.setCurrentItem(new_item)                   
        self.add_or_update_properties(properties)
        self.update_properties(self.tree.currentItem(), None)
        self.item_id_counter += 1

    def add_or_update_properties(self, properties):
        self.property_table.setRowCount(len(properties))
        for row, (key, value) in enumerate(properties.items()):
            key_item = QTableWidgetItem(key)
            value_item = QTableWidgetItem(str(value))
            self.property_table.setItem(row, 0, key_item)
            self.property_table.setItem(row, 1, value_item)
            
    def remove_dialog_properties(self, properties):
        print(f"remove {properties}")
        root = self.tree.invisibleRootItem()
        self.remove_dialog_properties_recursive(root, properties['id'])

    def remove_dialog_properties_recursive(self, parent_item, layerid):
        for i in range(parent_item.childCount()):
            child = parent_item.child(i)
            if child:  # Assurez-vous que child n'est pas None
                properties = child.data(0, Qt.UserRole)
                if properties and properties.get('id') == layerid:
                    parent_item.removeChild(child)
                    i -= 1  # Adjust index after removal
                else:
                    self.remove_dialog_properties_recursive(child, layerid)

    def resetTab(self):
        super().resetTab()
        self.property_table.clearContents()        

class AnimatedRect(QGraphicsRectItem):
    def __init__(self, x, y, w, h):
        super().__init__(x, y, w, h)
        self.setBrush(QColor("green"))

class AnimatableItem(QObject):
    def __init__(self, rect_item):
        super().__init__()
        self._rect_item = rect_item

    def getPos(self):
        return self._rect_item.pos()

    def setPos(self, pos):
        self._rect_item.setPos(pos)

    pos = pyqtProperty(QPointF, fget=getPos, fset=setPos)

class PathWindow(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Palette Window")
        self.setFixedSize(400, 300)
        
        # Layout principal
        main_layout = QHBoxLayout(self)
        
        # Layout gauche avec le bouton OK
        left_layout = QVBoxLayout()
        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.on_ok_clicked)
        left_layout.addWidget(self.ok_button)

        self.play_button = QPushButton("Play")
        self.play_button.clicked.connect(self.on_play_clicked)
        left_layout.addWidget(self.play_button)

        left_layout.addStretch()
        
        # Scène et vue graphique
        self.scene = QGraphicsScene()
        self.view = QGraphicsView(self.scene)
        self.view.setFixedSize(320, 200)
        
        # Ajouter le layout gauche et la vue graphique au layout principal
        main_layout.addLayout(left_layout)
        main_layout.addWidget(self.view)
        
        # Ajouter le point bleu au centre de la scène
        self.blue_point, self.blue_point_position = self.add_point(160, 100, "blue")
        
        # Rectangle à animer
        self.animated_rect = AnimatedRect(self.blue_point_position.x() - 5, self.blue_point_position.y() - 5, 10, 10)
        self.scene.addItem(self.animated_rect)
        
        # Objet animable
        self.animatable_item = AnimatableItem(self.animated_rect)
                 
        # Initialiser les variables pour les points et les lignes
        self.last_point = self.blue_point_position
        self.all_points = [self.blue_point_position]
        self.lines = []
        
        # Changer le curseur de la souris sur la vue
        self.view.setCursor(QCursor(Qt.CrossCursor))
        
        # Connecter l'événement de clic dans la scène
        self.view.mousePressEvent = self.on_scene_clicked

        # Centrer la vue sur le point bleu
        self.view.centerOn(self.blue_point)

    def add_point(self, x, y, color):
        radius = 5
        point = QGraphicsEllipseItem(x - radius, y - radius, radius * 2, radius * 2)
        point.setBrush(QColor(color))
        point.setPen(QPen(Qt.NoPen))
        self.scene.addItem(point)
        print("Added point at:", x, y)
        return point, QPointF(x, y)

    def add_line(self, point1_pos, point2_pos):
        line = QGraphicsLineItem(point1_pos.x(), point1_pos.y(), point2_pos.x(), point2_pos.y())
        line.setPen(QPen(QColor("black"), 2))
        self.scene.addItem(line)
        return line

    def on_scene_clicked(self, event):
        scene_pos = self.view.mapToScene(event.pos())
        print("Scene click position:", scene_pos.x(), scene_pos.y())
        item = self.scene.itemAt(scene_pos, self.view.transform())

        if item == self.blue_point or item in self.all_points:
            final_point, final_pos = self.add_point(scene_pos.x(), scene_pos.y(), "yellow")
            self.all_points.append(final_pos)
            print("Added yellow point at:", final_pos.x(), final_pos.y())
            new_line = self.add_line(self.last_point, final_pos)
            self.lines.append(new_line)
            self.last_point = final_pos            
        else:
            new_point, new_pos = self.add_point(scene_pos.x(), scene_pos.y(), "red")
            self.all_points.append(new_pos)
            print("Added red point at:", new_pos.x(), new_pos.y())
            new_line = self.add_line(self.last_point, new_pos)
            self.lines.append(new_line)
            self.last_point = new_pos
            
    def on_ok_clicked(self):
        blue_point_x = self.blue_point_position.x()
        blue_point_y = self.blue_point_position.y()

        all_points = [(point.x() - blue_point_x, point.y() - blue_point_y) for point in self.all_points]

        lines = [((line.line().x1() - blue_point_x, line.line().y1() - blue_point_y), 
                (line.line().x2() - blue_point_x, line.line().y2() - blue_point_y)) for line in self.lines]

        print("First Point: (0, 0)")
        print("All Points: ", all_points)
        print("Lines: ", lines)
        self.accept()

    def on_play_clicked(self):
        # Assurez-vous que le rectangle animé commence à la position du point bleu
        print("Position initiale du rectangle avant animation:", self.animatable_item.getPos())
        self.animatable_item.setPos(self.blue_point_position)
        print("Position après initialisation:", self.animatable_item.getPos())

        # Calculer les positions relatives
        blue_x, blue_y = self.blue_point_position.x(), self.blue_point_position.y()
        relative_points = [QPointF(point.x() - blue_x, point.y() - blue_y) for point in self.all_points]

        self.animation_group = QSequentialAnimationGroup()
        print("Nombre de points:", len(relative_points) - 1)
        for i in range(len(relative_points) - 1):
            start_pos = relative_points[i]
            end_pos = relative_points[i + 1]
            print(f"Animation de {start_pos} à {end_pos}")

            animation = QPropertyAnimation(self.animatable_item, b"pos")
            animation.setDuration(1000)
            animation.setStartValue(start_pos)
            animation.setEndValue(end_pos)
            animation.setEasingCurve(QEasingCurve.Linear)
            self.animation_group.addAnimation(animation)

        # Ajout d'un événement pour vérifier les positions après chaque animation
        def on_animation_finished():
            print("Position du rectangle après animation:", self.animatable_item.getPos())
            self.view.centerOn(self.animatable_item.getPos())  # Centrer la vue sur le rectangle animé

        self.animation_group.finished.connect(on_animation_finished)

        self.animation_group.start()

class SceneConfigurationDialog(QDialog):

    def __init__(self, parent=None):
    
        super(SceneConfigurationDialog, self).__init__(parent)
        self.initUI()

    def initUI(self):
    
        self.setWindowTitle('Scene Configuration Dialog')
        
        # Layout principal
        layout = QVBoxLayout(self)

        # Dictionnaire pour stocker les sliders et leurs étiquettes associées
        self.sliders = {}
        self.slider_labels = {}

        # List of specific labels and sliders
        slider_info = [
            ('Width', 0, 320, 1, 16),
            ('Height', 0, 224, 1, 16),
            ('Grid Size', 16, 64, 16, 16)
        ]

        for label_text, min_val, max_val, init_val, tick_interval in slider_info:
            slider_layout = QHBoxLayout()

            label = QLabel(label_text)
            slider = QSlider(Qt.Horizontal)
            slider.setMinimum(min_val)
            slider.setMaximum(max_val)
            slider.setValue(init_val)
            slider.setTickInterval(tick_interval)
            slider.setTickPosition(QSlider.TicksBelow)

            value_label = QLabel(str(init_val))
            self.slider_labels[slider] = value_label

            if label_text == 'Grid Size':
                slider.valueChanged.connect(self.update_grid_size)
            else:
                slider.valueChanged.connect(self.update_label)

            slider_layout.addWidget(label)
            slider_layout.addWidget(slider)

            if label_text == 'Grid Size':
                # Add the value label below the slider for "Grid Size"
                value_label.setAlignment(Qt.AlignCenter)
                vertical_layout = QVBoxLayout()
                vertical_layout.addLayout(slider_layout)
                vertical_layout.addWidget(value_label)
                layout.addLayout(vertical_layout)
            else:
                # Add the value label to the right of the slider for others
                slider_layout.addWidget(value_label)
                layout.addLayout(slider_layout)

            self.sliders[label_text] = slider


        # OK Button
        ok_button = QPushButton('OK')
        ok_button.clicked.connect(self.accept)
        layout.addWidget(ok_button)
        
    def update_grid_size(self, value):
        slider = self.sender()
        adjusted_value = round((value - 16) / 16) * 16 + 16
        slider.blockSignals(True)
        slider.setValue(adjusted_value)
        slider.blockSignals(False)
        value_label = self.slider_labels[slider]
        value_label.setText(str(adjusted_value))
        self.position_label(slider, value_label, 'Grid Size')
    
    def update_label(self):
        slider = self.sender()
        value_label = self.slider_labels[slider]
        value_label.setText(str(slider.value()))
        self.position_label(slider, value_label)
    
    def position_label(self, slider, label, label_type=None):
        if label_type == 'Grid Size':
            # Get the current value and maximum value from the slider
            value = slider.value()
            minimum = slider.minimum()
            maximum = slider.maximum()
    
            # Calculate handle position
            handle_position = (value - minimum) / (maximum - minimum)
    
            # Get slider geometry
            slider_rect = slider.geometry()
    
            # Calculate label position based on handle position
            label_x = slider_rect.x() + handle_position * slider_rect.width() - label.width() / 2
            label_y = slider_rect.y() + slider_rect.height() + 5  # 5 pixels below the slider
    
            # Set the label position
            label.move(int(label_x), int(label_y))
        else:
            # No additional positioning required for right-aligned labels
            pass
    
    def get_slider_values(self):
        return {label: slider.value() for label, slider in self.sliders.items()}

class SceneWidget(QMainWindow):

    gridToggle = pyqtSignal(bool)
    actionToggle = pyqtSignal(int)

    def __init__(self):
    
        super().__init__()
    
        self.title = 'PyQt5 zteam iotch'
        self.setWindowTitle(self.title)

        # Création du QWidget central qui contiendra le layout principal
        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        
        # Création et configuration du QVBoxLayout principal
        mainLayout = QVBoxLayout(centralWidget)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)
        
        self.initMenu()
        self.initSceneView()
        self.initTools()
        self.initWelcome()
        
        self.buildPack(mainLayout)

    def initMenu(self):
    
        # Configuration de la barre de menu
        self.menuBar = self.menuBar()
        
        # Ajout de menus à la barre de menu
        fileMenu = self.menuBar.addMenu('File')
        
        # Ajout d'actions au menu File
        
        newAction = QAction('New', self)
        newAction.setShortcut('Ctrl+O')
        newAction.triggered.connect(self.newDialog)
        fileMenu.addAction(newAction)
        
        openAction = QAction('Import', self)
        openAction.setShortcut('Ctrl+O')
        openAction.triggered.connect(self.importDialog)
        fileMenu.addAction(openAction)

        saveAction = QAction('Export', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.triggered.connect(self.exportDialog)
        fileMenu.addAction(saveAction)

        exitAction = QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(sys.exit)
        fileMenu.addAction(exitAction)
    
        # ToolBar ajouté directement au QMainWindow
        self.toolBar = QToolBar("Main toolbar")
        self.addToolBar(self.toolBar)
        self.toolBar.addAction(exitAction)        
        
        # Créer un QToolButton en tant que bouton à bascule
        self.gridToggleBtn = QToolButton()
        self.gridToggleBtn.setText("Grid")
        self.gridToggleBtn.setCheckable(True)
        self.gridToggleBtn.setChecked(True)
        
        # Configuration des feuilles de style
        self.gridToggleBtn.setStyleSheet("""
            QPushButton {
                background-color: lightgray;
                border: 2px solid gray;
                border-radius: 5px;
            }
            QPushButton:checked {
                background-color: lightgreen;
                border-color: green;
            }
        """)        
        # Ajouter le QToolButton à la QToolBar
        self.toolBar.addWidget(self.gridToggleBtn)

        # Connecter le signal toggled pour surveiller l'état du bouton à bascule
        self.gridToggleBtn.toggled.connect(self.on_gridToggleBtn)

        # Créer un QToolButton en tant que bouton à bascule
        self.tileToggleBtn = QToolButton()
        self.tileToggleBtn.setText("TilesActive")
        self.tileToggleBtn.setCheckable(True)
        self.tileToggleBtn.setChecked(True)
        self.tileToggleBtn.toggled.connect(self.on_actionToggleBtn)

        # Configuration des feuilles de style
        self.tileToggleBtn.setStyleSheet("""
            QPushButton {
                background-color: lightgray;
                border: 2px solid gray;
                border-radius: 5px;
            }
            QPushButton:checked {
                background-color: lightgreen;
                border-color: green;
            }
        """)        
        # Ajouter le QToolButton à la QToolBar
        self.toolBar.addWidget(self.tileToggleBtn)

        self.spriteToggleBtn = QToolButton()
        self.spriteToggleBtn.setText("SpritesActive")
        self.spriteToggleBtn.setCheckable(True)
        self.spriteToggleBtn.setChecked(False)
        self.spriteToggleBtn.toggled.connect(self.on_actionToggleBtn)
        
        # Configuration des feuilles de style
        self.spriteToggleBtn.setStyleSheet("""
            QPushButton {
                background-color: lightgray;
                border: 2px solid gray;
                border-radius: 5px;
            }
            QPushButton:checked {
                background-color: lightgreen;
                border-color: green;
            }
        """)        
        # Ajouter le QToolButton à la QToolBar
        self.toolBar.addWidget(self.spriteToggleBtn)

    def initSceneView(self):

        self.scene = QGraphicsScene()               
        self.view = MyGraphicsView(self.scene, self)
            
        ## view integre qscrollarea
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidget(self.view)
        self.scrollArea.setWidgetResizable(True)
                
    def initTools(self):
    
        # Création du QTreeView
        self.treeView = QTreeView()
        
        self.model = QFileSystemModel()
        self.model.setRootPath('')
        self.treeView.setModel(self.model)
        
        # Définir le chemin du répertoire racine selon les besoins
        self.treeView.setRootIndex(self.model.index('..'))  
        self.treeView.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

        # Ajout d'un écouteur pour le double-clic
        self.treeView.doubleClicked.connect(self.openPathWindow)
    
        # Création du QTabWidget
        self.toolBox = QTabWidget()
                
        self.layer_tab = LayerTabStandard("Layers", self.scene, self.view)
        self.tileset_tab = TilesetTabWithFileDialog("Tilesets")
        self.sprite_tab = SpriteTab("Sprites")
        self.spriteTemplate = SpriteTemplateTab("Templates", self.scene, self.view)
        
        self.toolBox.addTab(self.layer_tab, "Layers")
        self.toolBox.addTab(self.tileset_tab, "Tilesets")                
        self.toolBox.addTab(self.sprite_tab, "Sprites")                
        self.toolBox.addTab(self.spriteTemplate, "Templates")
        self.toolBox.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
 
         # Création du QSplitter vertical
        self.verticalSplitter = QSplitter(Qt.Vertical)
        self.verticalSplitter.addWidget(self.treeView)
        self.verticalSplitter.addWidget(self.toolBox)
        
        # Optionnel: Définir les proportions initiales (poids) des widgets
        self.verticalSplitter.setStretchFactor(0, 4)
        self.verticalSplitter.setStretchFactor(1, 6)
        
        self.layer_tab.LayerUpdatedProps.connect(self.view.updateSceneLayer)        
        self.layer_tab.LayerDeleted.connect(self.view.deleteSceneLayer)
        
        self.tileset_tab.tile_widget_manager.tileSelectionChanged.connect(self.view.setSelectedTiles)
        
        self.view.spriteAdded.connect(self.sprite_tab.handle_new_sprite_properties)
        self.view.spriteDeleted.connect(self.sprite_tab.remove_dialog_properties)
        
        self.sprite_tab.spriteDeleted.connect(self.view.deleteSceneSprite)

        self.gridToggle.connect(self.view.toggleSceneGrid)
        self.actionToggle.connect(self.view.toggleMouseEvent)

    def initWelcome(self):
        pass
        
    def buildPack(self, layout):
    
        self.mainSplitter  = QSplitter(Qt.Horizontal)
        
        self.mainSplitter.addWidget(self.verticalSplitter)
        self.mainSplitter.addWidget(self.scrollArea)
        
        layout.addWidget(self.mainSplitter )
        
    def newDialog(self):
        sceneConfigurationDialog  = SceneConfigurationDialog(self)
        if sceneConfigurationDialog.exec_() == QDialog.Accepted:
            values = sceneConfigurationDialog.get_slider_values()
            print("Slider values:", values)
            configuration['exists'] = True
            configuration['map_width'] = values['Width']*values['Grid Size']
            configuration['map_height'] = values['Height']*values['Grid Size']
            self.layer_tab.add_layer_dialog_configuration()
    
    def importDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "Open File", "", "JSON Files (*.json);;All Files (*)", options=options)
        if fileName:
            with open(fileName, 'r') as file:
                data = json.load(file)
            print(f"Data successfully imported from {fileName}")

            new_configuration = data.get("configuration", {})
            self.mergeConfiguration(new_configuration)

            self.layer_tab.resetTab()
            self.tileset_tab.resetTab()
            self.sprite_tab.resetTab()
            self.spriteTemplate.resetTab()

            # Importer les données dans chaque tab
            self.importProperties(self.layer_tab, data.get("layers", []))
            self.importProperties(self.tileset_tab, data.get("tilesets", []))
            self.importProperties(self.sprite_tab, data.get("sprites", []))
            self.importProperties(self.spriteTemplate, data.get("templates", []))


    def mergeConfiguration(self, new_configuration):
        global configuration
        configuration.update(new_configuration)
           
    def importProperties(self, tab, properties_list):
        item = 0
        for properties in properties_list:
            # Si le tab est tileset_tab, appelez également create_tile_widget
            if isinstance(tab, TilesetTabWithFileDialog) or isinstance(tab, SpriteTemplateTab):
                file_path = properties.get("File Path")
                item_name = file_path.split('/')[-1]
                tab.tree_manager.add_layer(item_name, properties)
                
                if isinstance(tab, TilesetTabWithFileDialog):
                    tile_size = properties.get("tile_size", 32)
                    if file_path:
                        tab.tile_widget_manager.create_tile_widget(file_path, tile_size)
            elif isinstance(tab, SpriteTab):
                item_name = "Sprite " + str(properties.get("id"))
                tab.tree_manager.add_layer(item_name, properties)
            elif isinstance(tab, LayerTab):
                item_name = "Layer " + str(properties.get("id"))
                tab.tree_manager.add_layer(item_name, properties)

            item += 1

    def exportDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self, "Save File", "", "JSON Files (*.json);;All Files (*)", options=options)
        if fileName:
            # Collecter les propriétés de chaque item de chaque tab
            data = {
                "layers": self.collectProperties(self.layer_tab),
                "tilesets": self.collectProperties(self.tileset_tab),
                "sprites": self.collectProperties(self.sprite_tab),
                "templates": self.collectProperties(self.spriteTemplate),
                "configuration": configuration
            }
            # Écrire les données dans le fichier JSON
            with open(fileName, 'w') as file:
                json.dump(data, file, indent=4)
            print(f"Data successfully exported to {fileName}")

    def collectProperties(self, tab):
        properties_list = []
        root = tab.tree.invisibleRootItem()
        self.collectPropertiesRecursive(root, properties_list)
        return properties_list

    def collectPropertiesRecursive(self, parent_item, properties_list):
        for i in range(parent_item.childCount()):
            child = parent_item.child(i)
            properties = child.data(0, Qt.UserRole)
            if properties:
                properties_list.append(properties)
            self.collectPropertiesRecursive(child, properties_list)


    def openPaletteWindow(self, index):
        '''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All PNG (*.png)", options=options)
        if fileName:
            print(fileName)
        '''
        fileName = self.model.fileName(index)
        self.image = QImage(fileName)

        self.scene.addPixmap(QPixmap.fromImage(self.image))

        self.paletteWindow = ColorPaletteWindow(self.image, self.scene)
        self.paletteWindow.show()            

    def openPathWindow(self, index):
        fileName = self.model.fileName(index)
        
        self.pathWindow = PathWindow()
        self.pathWindow.show()       

    def on_gridToggleBtn(self, checked):
        self.gridToggle.emit(checked)

    def on_actionToggleBtn(self, checked):
        sender = self.sender()
        if checked:
            if sender == self.tileToggleBtn:
                self.spriteToggleBtn.setChecked(False)
                #info = {"id": 1, "name": "Button 1", "status": "active"}
                self.actionToggle.emit(1)
            elif sender == self.spriteToggleBtn:
                self.tileToggleBtn.setChecked(False)
                self.actionToggle.emit(2)
            else:
                self.actionToggle.emit(0)

    def resizeEvent(self, event):
        print("Resizing - now recalculating layout")

        totalWidth = self.mainSplitter.width()
        leftWidth = int(totalWidth * 0.2)  # 20% de l'espace
        rightWidth = int(totalWidth * 0.8)  # 80% de l'espace
        self.mainSplitter.setSizes([leftWidth, rightWidth])

        super().resizeEvent(event)  # Assurez-vous de laisser cet appel une seule fois

        # Assurez-vous que la méthode recalculateLayout est définie dans votre classe ou supprimez cet appel
        if hasattr(self, 'recalculateLayout'):
            self.recalculateLayout()
            

# Point d'entrée de l'application
def main():
    app = QApplication(sys.argv)
    window = SceneWidget()
    window.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
