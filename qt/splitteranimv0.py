import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QFileDialog, QPushButton, QVBoxLayout, QHBoxLayout, QGridLayout, QMessageBox
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QTimer

class ImageGrid(QWidget):
    def __init__(self, block_size=16, zoom_factor=4):
        super().__init__()
        self.block_size = block_size
        self.zoom_factor = zoom_factor
        self.image_path = self.open_file_dialog()
        self.mouse_move_active = False  # Nouvelle variable pour contrôler mouseMoveEvent
        if self.image_path:
            self.initUI()

    def initUI(self):
        self.setWindowTitle('Image Grid Interaction')
        self.current_button = None
        self.blocks_dict = {}
        self.positions_dict = {}
        self.current_positions_dict = {}

        # Créer un layout principal
        main_layout = QVBoxLayout(self)

        # Ajouter les boutons centrés
        button_layout = QHBoxLayout()
        button_layout.addStretch(1)

        btn1 = QPushButton('Button 1', self)
        btn1.clicked.connect(lambda: self.activate_blocks('btn1'))
        button_layout.addWidget(btn1)

        btn2 = QPushButton('Button 2', self)
        btn2.clicked.connect(lambda: self.activate_blocks('btn2'))
        button_layout.addWidget(btn2)

        btn3 = QPushButton('Button 3', self)
        btn3.clicked.connect(lambda: self.activate_blocks('btn3'))
        button_layout.addWidget(btn3)

        button_layout.addStretch(1)

        main_layout.addLayout(button_layout)

        # Créer un widget pour contenir les blocs d'image
        self.image_widget = QWidget(self)
        self.image_layout = QGridLayout(self.image_widget)
        self.image_layout.setSpacing(0)

        # Charger l'image
        self.pixmap = QPixmap(self.image_path)

        # Taille de l'image et calcul du nombre de blocs
        self.img_width = self.pixmap.width()
        self.img_height = self.pixmap.height()
        self.num_blocks_x = self.img_width // self.block_size
        self.num_blocks_y = self.pixmap.height() // self.block_size

        # Initialiser et stocker les positions initiales des blocs pour chaque bouton
        for btn_name in ['btn1', 'btn2', 'btn3']:
            self.store_initial_positions(btn_name)

        # Initialiser la vue avec les blocs du bouton 1
        self.activate_blocks('btn1')

        # Ajouter le widget d'image au layout principal
        main_layout.addWidget(self.image_widget)

        # Taille de la fenêtre (facteur de zoom)
        self.setGeometry(100, 100, self.num_blocks_x * self.block_size * self.zoom_factor,
                         self.num_blocks_y * self.block_size * self.zoom_factor + 50)

    def open_file_dialog(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "Sélectionner un fichier d'image", "", "Images (*.png *.xpm *.jpg)", options=options)
        return file_name

    def store_initial_positions(self, btn_name):
        self.blocks_dict[btn_name] = [[self.create_block_label(x, y) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]
        self.positions_dict[btn_name] = [[(x * self.block_size * self.zoom_factor, y * self.block_size * self.zoom_factor) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]
        self.current_positions_dict[btn_name] = [[(x * self.block_size * self.zoom_factor, y * self.block_size * self.zoom_factor) for x in range(self.num_blocks_x)] for y in range(self.num_blocks_y)]

        # Log initial positions
        print(f"Initial positions stored for {btn_name}: {self.current_positions_dict[btn_name][0][0]}")

    def create_block_label(self, x, y):
        label = QLabel(self)
        block_pixmap = self.pixmap.copy(x * self.block_size, y * self.block_size, self.block_size, self.block_size)
        label.setPixmap(block_pixmap.scaled(self.block_size * self.zoom_factor, self.block_size * self.zoom_factor))
        label.setStyleSheet("border: 1px solid red;")
        
        return label

    def activate_blocks(self, btn_name):
        if self.current_button is not None:
            self.update_current_positions()
        
        self.current_button = btn_name

        # Activer ou désactiver la fonction mouseMoveEvent
        if btn_name in ['btn2', 'btn3']:
            self.mouse_move_active = True
        else:
            self.mouse_move_active = False
            self.animate_blocks('btn2', 'btn3')  # Animation lors du clic sur btn1

        self.update_image_layout()

        # Log after activation
        print(f"After activation of {btn_name}: {self.current_positions_dict[btn_name][0][0]}")

        # Forcer la mise à jour de l'interface utilisateur
        self.image_widget.repaint()
        self.image_widget.update()

    def update_current_positions(self):
        if self.current_button:
            self.current_positions_dict[self.current_button] = [[(label.x(), label.y()) for label in row] for row in self.blocks_dict[self.current_button]]
            
            # Log current positions
            print(f"Updated positions for {self.current_button}: {self.current_positions_dict[self.current_button][0][0]}")

    def update_image_layout(self):
        for i in reversed(range(self.image_layout.count())):
            widget = self.image_layout.itemAt(i).widget()
            self.image_layout.removeWidget(widget)
            widget.setParent(None)

        
        active_blocks = self.blocks_dict[self.current_button]
        for y, row in enumerate(active_blocks):
            for x, label in enumerate(row):
                self.image_layout.addWidget(label, y, x)

        #self.show_message_dialog()
        QApplication.processEvents()

        for y, row in enumerate(active_blocks):
            for x, label in enumerate(row):           
                pos = self.current_positions_dict[self.current_button][y][x]
                label.move(*pos)
                label.lower()

        # Log after layout update
        print(f"Layout updated for {self.current_button}: {self.current_positions_dict[self.current_button][0][0]}")

        # Forcer la mise à jour de l'interface utilisateur
        self.image_widget.update()
        self.image_widget.repaint()

    def animate_blocks(self, from_btn, to_btn):
        from_positions = self.current_positions_dict[from_btn]
        to_positions = self.current_positions_dict[to_btn]
        steps = 10
        self.animation_step = 0
        self.animation_timer = QTimer(self)
        self.animation_timer.timeout.connect(lambda: self.perform_animation_step(from_positions, to_positions, steps))
        self.animation_timer.start(50)

    def perform_animation_step(self, from_positions, to_positions, steps):
        t = (self.animation_step + 1) / steps
        for y in range(self.num_blocks_y):
            for x in range(self.num_blocks_x):
                start_pos = from_positions[y][x]
                end_pos = to_positions[y][x]
                new_x = int(start_pos[0] * (1 - t) + end_pos[0] * t)
                new_y = int(start_pos[1] * (1 - t) + end_pos[1] * t)
                self.blocks_dict['btn1'][y][x].move(new_x, new_y)

        self.image_widget.update()
        self.image_widget.repaint()

        self.animation_step += 1
        if self.animation_step >= steps:
            self.animation_timer.stop()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.start_x = event.x()
            self.start_y = event.y()

    def mouseMoveEvent(self, event):
        if self.mouse_move_active and event.buttons() & Qt.LeftButton:
            current_x = event.x()
            current_y = event.y()
            delta_x = current_x - self.start_x
            delta_y = current_y - self.start_y
            self.start_x = current_x
            self.start_y = current_y

            active_blocks = self.blocks_dict[self.current_button]
            for col in range(self.num_blocks_x):
                for row in range(self.num_blocks_y):
                    factor = col + 1
                    old_x = active_blocks[row][col].x()
                    new_x = old_x + delta_x * factor
                    max_x = self.num_blocks_x * self.block_size * self.zoom_factor - (self.num_blocks_x - col) * self.block_size * self.zoom_factor
                    if new_x > max_x:
                        new_x = max_x
                    active_blocks[row][col].move(new_x, active_blocks[row][col].y())
                    active_blocks[row][col].lower()

                    factor = row + 1
                    old_y = active_blocks[row][col].y()
                    new_y = old_y + delta_y * factor
                    max_y = self.num_blocks_y * self.block_size * self.zoom_factor - (self.num_blocks_y - row) * self.block_size * self.zoom_factor
                    if new_y > max_y:
                        new_y = max_y
                    active_blocks[row][col].move(active_blocks[row][col].x(), new_y)
                    active_blocks[row][col].lower()

            self.update_current_positions()

            # Forcer la mise à jour de l'interface utilisateur
            self.image_widget.update()
            self.image_widget.repaint()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ImageGrid()
    ex.show()
    sys.exit(app.exec_())
