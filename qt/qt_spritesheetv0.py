import sys, os

from PyQt5.QtWidgets import QApplication, QWidget, QFileDialog, QMenuBar, QToolBar, QLabel, QPushButton, QDialog, QSlider, QSizePolicy, QSplitter, QScrollArea

from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTreeView, QTreeWidget, QTreeWidgetItem
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem

from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView, QGraphicsRectItem, QGraphicsEllipseItem
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QHBoxLayout, QFileSystemModel, QAction

from PyQt5.QtGui import QImage, QBrush, QPainter, QPen, QPixmap, QPolygonF, QColor, QDrag
from PyQt5.QtCore import Qt, QMimeData, QFileInfo

class DraggableColorLabel(QLabel):
    def __init__(self, color):
        super().__init__()
        self.setFixedSize(30, 30)
        #self.color = color
        if isinstance(color, QColor):
            self.color = color
        else:
            self.color = QColor(color)        
        #self.setStyleSheet(f"background-color: {QColor(color).name()}; border: 1px solid black;")
        self.updateStyleSheet()
        self.setAcceptDrops(True)

    def updateStyleSheet(self):
        self.setStyleSheet(f"background-color: {self.color.name()}; border: 1px solid black;")

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_start_position = event.pos()

    def mouseMoveEvent(self, event):
        if not (event.buttons() & Qt.LeftButton):
            return
        if (event.pos() - self.drag_start_position).manhattanLength() < QApplication.startDragDistance():
            return

        drag = QDrag(self)
        mimedata = QMimeData()
        mimedata.setColorData(self.color)
        drag.setMimeData(mimedata)
        pixmap = QPixmap(self.size())
        self.render(pixmap)
        drag.setPixmap(pixmap)
        drag.setHotSpot(event.pos())
        drag.exec_(Qt.MoveAction)

    def dragEnterEvent(self, event):
        if event.mimeData().hasColor():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        new_color = event.mimeData().colorData()
        old_color = self.color
        self.color = new_color
        self.setStyleSheet(f"background-color: {QColor(new_color).name()}; border: 1px solid black;")
        event.source().color = old_color
        event.source().setStyleSheet(f"background-color: {QColor(old_color).name()}; border: 1px solid black;")

    def mouseDoubleClickEvent(self, event):
        self.openColorAdjuster()

    def openColorAdjuster(self):
        self.color_window = ColorAdjustWindow(self.color, self)
        self.color_window.show()

    def updateColor(self, new_color):
        if isinstance(new_color, QColor):
            self.color = new_color
        else:
            self.color = QColor(new_color)
        self.updateStyleSheet()

class ColorAdjustWindow(QWidget):
    def __init__(self, color, parent=None):
        super().__init__()
        self.color = color
        self.initUI()
        self.parent_label = parent
    def initUI(self):
        main_layout = QVBoxLayout(self)

        self.color_preview = QLabel()
        self.color_preview.setStyleSheet("background-color: " + self.color.name())
        self.color_preview.setFixedHeight(30)  # Fixe seulement la hauteur
        self.color_preview.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        main_layout.addWidget(self.color_preview)
        
        # Disposition des sliders et des labels hexadécimaux
        sliders_layout = QHBoxLayout()

        self.sliders = []
        self.hex_labels = []
        labels = ['Red', 'Green', 'Blue']
        for i, comp in enumerate([self.color.red(), self.color.green(), self.color.blue()]):
            # Conteneur vertical pour chaque slider et son label
            slider_container = QVBoxLayout()

            slider = QSlider(Qt.Vertical)
            slider.setMaximum(255)
            slider.setValue(comp)
            slider.valueChanged.connect(lambda value, idx=i, s=self: s.updateColor(idx, value))

            label = QLabel(f"{labels[i]}: {comp:02X}")
            label.setAlignment(Qt.AlignCenter)  # Centre le texte sous le slider

            slider_container.addWidget(slider)
            slider_container.addWidget(label)

            sliders_layout.addLayout(slider_container)
            self.sliders.append(slider)
            self.hex_labels.append(label)

        main_layout.addLayout(sliders_layout)

        btn_ok = QPushButton('OK')
        btn_ok.clicked.connect(self.applyChanges)
        main_layout.addWidget(btn_ok)

    def updateColor(self, index, value):
        # Mise à jour de la couleur
        if index == 0:
            self.color.setRed(value)
        elif index == 1:
            self.color.setGreen(value)
        elif index == 2:
            self.color.setBlue(value)

        # Mise à jour de l'aperçu de couleur et du label hexadécimal
        self.color_preview.setStyleSheet("background-color: " + self.color.name())
        self.hex_labels[index].setText(f"{['Red', 'Green', 'Blue'][index]}: {value:02X}")

    def applyChanges(self):
        if self.parent_label:
            self.parent_label.updateColor(self.color)
        self.close()

class ColorPaletteWindow(QWidget):
    def __init__(self, image, scene):#color_table, pixmap_item):
        super().__init__()
        self.image = image
        self.scene = scene        
        self.initUI()
        
    def initUI(self):
        layout = QGridLayout(self)
        self.labels = []
        for i, color in enumerate(self.image.colorTable()):#self.color_table):
            label = DraggableColorLabel(color)
            self.labels.append(label)
            layout.addWidget(label, i // 10, i % 10)

        btn_ok = QPushButton('OK', self)
        btn_ok.clicked.connect(self.updateImage)
        layout.addWidget(btn_ok, len(self.labels) // 10 + 1, 0, 1, 10)

        self.setWindowTitle('Palette de couleurs')
        self.setLayout(layout)

    def updateImage(self):
        for i, label in enumerate(self.labels):
            self.image.setColor(i, QColor(label.color).rgb())
        self.image = self.image.convertToFormat(QImage.Format_Indexed8)
        pixmap = QPixmap.fromImage(self.image)
        self.scene.addPixmap(pixmap)
        self.close()

class LayerTab(QWidget):
    def __init__(self, tab_name="Layers"):
        super().__init__()
        self.tab_name = tab_name

        layout = QVBoxLayout()
        self.setLayout(layout)

        # ToolBar with actions
        self.toolbar = QToolBar()
        layout.addWidget(self.toolbar)

        # Setup Actions
        self.setup_actions()

        # Splitter to hold TreeView and Property Table, allowing resizing
        self.splitter = QSplitter(Qt.Vertical)
        layout.addWidget(self.splitter)

        # TreeView
        self.tree = QTreeWidget()
        self.tree.setHeaderLabel(self.tab_name)
        self.splitter.addWidget(self.tree)  # Add tree to splitter

        # Property Table
        self.property_table = QTableWidget(2, 2)
        self.property_table.setHorizontalHeaderLabels(['Key', 'Value'])
        self.property_table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.splitter.addWidget(self.property_table)  # Add property table to splitter

        self.tree.currentItemChanged.connect(self.update_properties)

    def setup_actions(self):
        self.add_action = QAction('+', self)
        self.add_action.triggered.connect(self.add_item)
        self.toolbar.addAction(self.add_action)

        self.remove_action = QAction('-', self)
        self.remove_action.triggered.connect(self.remove_item)
        self.toolbar.addAction(self.remove_action)

        self.up_action = QAction('↑', self)
        self.up_action.triggered.connect(self.move_item_up)
        self.toolbar.addAction(self.up_action)

        self.down_action = QAction('↓', self)
        self.down_action.triggered.connect(self.move_item_down)
        self.toolbar.addAction(self.down_action)

    def add_item(self):
        new_index = self.tree.topLevelItemCount() + 1
        item = QTreeWidgetItem([f"Layer {new_index}"])
        self.tree.addTopLevelItem(item)
        item.setData(0, Qt.UserRole, {"Visible": "True", "Index": new_index})
        self.tree.setCurrentItem(item)

    def remove_item(self):
        item = self.tree.currentItem()
        if item:
            index = self.tree.indexOfTopLevelItem(item)
            self.tree.takeTopLevelItem(index)

    def move_item_up(self):
        current_index = self.tree.currentIndex().row()
        if current_index > 0:
            item = self.tree.takeTopLevelItem(current_index)
            self.tree.insertTopLevelItem(current_index - 1, item)
            self.tree.setCurrentItem(item)
            self.update_index()
            self.update_properties(item, None)

    def move_item_down(self):
        current_index = self.tree.currentIndex().row()
        if current_index < self.tree.topLevelItemCount() - 1:
            item = self.tree.takeTopLevelItem(current_index)
            self.tree.insertTopLevelItem(current_index + 1, item)
            self.tree.setCurrentItem(item)
            self.update_index()
            self.update_properties(item, None)

    def update_properties(self, current, previous):
        if current:
            # Clear the existing properties
            self.property_table.clearContents()
            self.property_table.setRowCount(0)

            # Get the properties stored in the item
            properties = current.data(0, Qt.UserRole)
            if properties:
                # Update the property table with the properties of the current item
                row_count = len(properties)
                self.property_table.setRowCount(row_count)
                for index, (key, value) in enumerate(properties.items()):
                    self.property_table.setItem(index, 0, QTableWidgetItem(key))
                    self.property_table.setItem(index, 1, QTableWidgetItem(str(value)))

    def update_index(self):
        for i in range(self.tree.topLevelItemCount()):
            item = self.tree.topLevelItem(i)
            properties = item.data(0, Qt.UserRole)
            properties["Index"] = i + 1
            item.setData(0, Qt.UserRole, properties)

class TileWidget(QWidget):
    def __init__(self, image_path, tile_size):
        super().__init__()
        self.file_path = image_path
        self.tile_size = tile_size
        self.init_ui()

    def init_ui(self):
        # Configuration du layout pour contenir les tiles
        self.layout = QGridLayout(self)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)

        # Charger l'image et initialiser les tiles
        self.set_image(self.file_path, self.tile_size)

    def set_image(self, image_path, tile_size):
        pixmap = QPixmap(image_path)
        if pixmap.isNull():
            print("Erreur : Image non chargée.")
            return

        rows = pixmap.height() // tile_size
        cols = pixmap.width() // tile_size

        # Calculer la taille totale requise
        total_width = cols * tile_size
        total_height = rows * tile_size

        # Fixer la taille maximale du widget
        self.setFixedSize(total_width, total_height)

        for i in reversed(range(self.layout.count())): 
            widget = self.layout.itemAt(i).widget()
            if widget is not None:
                widget.setParent(None)

        for row in range(rows):
            for col in range(cols):
                label = QLabel()
                label.setPixmap(pixmap.copy(col * tile_size, row * tile_size, tile_size, tile_size))
                self.layout.addWidget(label, row, col)
                
        # Vider le layout existant
        #for i in reversed(range(self.layout.count())): 
        #    widget_to_remove = self.layout.itemAt(i).widget()
        #    if widget_to_remove:
        #        self.layout.removeWidget(widget_to_remove)
        #        widget_to_remove.setParent(None)
        #        widget_to_remove.deleteLater()
        #
        ## Ajouter les tiles au layout
        #for row in range(rows):
        #    for col in range(cols):
        #        tile = pixmap.copy(col * tile_size, row * tile_size, tile_size, tile_size)
        #        label = QLabel()
        #        label.setPixmap(tile)
        #        label.setFixedSize(tile_size, tile_size)
        #        label.setStyleSheet("border: 1px solid black; margin:0px; padding:0px;")
        #        self.layout.addWidget(label, row, col)

class LayerTabWithFileDialog(LayerTab):
    def __init__(self, tab_name="Layers"):
        # S'assurer que toute initialisation dans LayerTab est complétée
        super().__init__(tab_name)  

        # Initialisation des attributs spécifiques à LayerTabWithFileDialog
        self.tile_widgets = {}
        self.item_id_counter = 0  

        # Détacher l'action existante et attacher une nouvelle action
        self.add_action.triggered.disconnect()
        self.add_action.triggered.connect(self.open_add_file_dialog)

        # Création et configuration de l'objet QScrollArea
        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.tiles_widget = QWidget()
        self.tiles_layout = QGridLayout()
        self.tiles_widget.setLayout(self.tiles_layout)
        self.scroll_area.setWidget(self.tiles_widget)
        self.layout().addWidget(self.scroll_area)
    
        # Connecter le signal currentItemChanged de l'arbre à display_tile_widget
        self.tree.currentItemChanged.connect(self.display_tile_widget)
    
    def open_add_file_dialog(self):
        self.dialog = QWidget()
        layout = QVBoxLayout(self.dialog)

        # File label and open button
        file_layout = QHBoxLayout()
        self.file_label = QLabel("No file selected")
        open_button = QPushButton("Open File")
        open_button.clicked.connect(self.open_file)
        file_layout.addWidget(self.file_label)
        file_layout.addWidget(open_button)

        # Slider
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(16)  # Minimum value
        self.slider.setMaximum(64)  # Maximum value
        self.slider.setSingleStep(16)
        self.slider.setTickInterval(16)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setValue(16)  # Default value
        self.slider_label = QLabel("16")  # Default value
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider_label = QLabel("16")  # default value
        self.slider.valueChanged.connect(self.update_slider_label)
        slider_layout = QHBoxLayout()
        slider_layout.addWidget(self.slider)
        slider_layout.addWidget(self.slider_label)

        # OK Button
        ok_button = QPushButton("OK")
        ok_button.clicked.connect(self.close_dialog_and_add)

        layout.addLayout(file_layout)
        layout.addLayout(slider_layout)
        layout.addWidget(ok_button)

        self.dialog.setWindowTitle("Add File")
        self.dialog.setGeometry(100, 100, 400, 200)
        self.dialog.show()

    def open_file(self):
        file_path, _ = QFileDialog.getOpenFileName(self.dialog, "Open File")
        if file_path:
            file_name = file_path.split('/')[-1]
            self.file_label.setText(file_name)
            self.file_label.setToolTip(file_path)  # Store the full file path as tooltip

    def update_slider_label(self, value):
        # Adjust slider value to the nearest multiple of 16
        adjusted_value = round((value - 16) / 16) * 16 + 16
        self.slider.setValue(adjusted_value)  # Set the slider to the adjusted value
        self.slider_label.setText(str(adjusted_value))

    def close_dialog_and_add(self):
        item_name = self.file_label.text()  # Récupérer le nom du fichier choisi
        if item_name:
            tile_value = int(self.slider_label.text())  # Récupérer la valeur du slider
            file_path = self.file_label.toolTip()  # Récupérer le chemin complet du fichier
            if file_path and tile_value:
                # Créer un nouvel élément pour le QTreeWidget avec le nom du fichier
                new_item = QTreeWidgetItem([item_name])
                
                unique_id = self.item_id_counter  # Utiliser un identifiant unique pour chaque élément
                self.item_id_counter += 1  # Incrémenter l'identifiant pour le prochain élément

                # Stocker des données supplémentaires avec l'élément de l'arbre
                new_item.setData(0, Qt.UserRole, {
                    "Visible": "True",
                    "Index": unique_id
                })
                self.tree.addTopLevelItem(new_item)  # Ajouter l'élément au QTreeWidget
                self.tree.setCurrentItem(new_item)  # Définir l'élément comme sélectionné

                # Créer le widget correspondant au fichier et à la valeur du slider
                tile_widget = TileWidget(file_path, tile_value)
                self.tile_widgets[unique_id] = tile_widget  # Stocker ce widget dans un dictionnaire

                # Ajouter le widget au layout, initialement non visible
                tile_widget.setVisible(False)
                self.tiles_layout.addWidget(tile_widget, unique_id, 0)  # Positionner le widget dans le grid layout

                # Afficher le widget correspondant à l'élément actuellement sélectionné
                self.display_tile_widget(new_item, None)

        self.dialog.close()  # Fermer le dialogue


    def display_tile_widget(self, current, previous):
        if current is None:
            return

        # Hide previous widget
        if previous:
            previous_id = previous.data(0, Qt.UserRole).get("Index")
            if previous_id in self.tile_widgets:
                self.tile_widgets[previous_id].setVisible(False)

        # Show current widget
        item_id = current.data(0, Qt.UserRole).get("Index")
        if item_id in self.tile_widgets:
            self.tile_widgets[item_id].setVisible(True)
        else:
            print("Aucun widget trouvé pour l'item sélectionné")
 
    def move_item_up(self):
        current_item = self.tree.currentItem()
        if current_item is None:
            print("Aucun élément sélectionné.")
            return

        index = self.tree.indexOfTopLevelItem(current_item)
        if index > 0:
            # Retirer l'item et le réinsérer à la position précédente
            self.tree.takeTopLevelItem(index)
            self.tree.insertTopLevelItem(index - 1, current_item)
            self.tree.setCurrentItem(current_item)

            # Mettre à jour le layout des widgets
            self.update_widgets_order()

            # Restaurer l'état du scroll_area si nécessaire
            # Exemple: self.scroll_area.verticalScrollBar().setValue(desired_scroll_position)

    def move_item_down(self):
        current_item = self.tree.currentItem()
        if current_item is None:
            print("Aucun élément sélectionné.")
            return

        index = self.tree.indexOfTopLevelItem(current_item)
        total_items = self.tree.topLevelItemCount()
        if index < total_items - 1:
            # Retirer l'item et le réinsérer à la position suivante
            self.tree.takeTopLevelItem(index)
            self.tree.insertTopLevelItem(index + 1, current_item)
            self.tree.setCurrentItem(current_item)

            # Mettre à jour le layout des widgets
            self.update_widgets_order()

            # Restaurer l'état du scroll_area si nécessaire
            # Exemple: self.scroll_area.verticalScrollBar().setValue(desired_scroll_position)

    def update_widgets_order(self):
        for i in range(self.tree.topLevelItemCount()):
            item = self.tree.topLevelItem(i)
            item_id = item.data(0, Qt.UserRole).get("Index")
            widget = self.tile_widgets[item_id]
            # Retirer le widget du layout et le réajouter à la nouvelle position
            self.tiles_layout.removeWidget(widget)
            self.tiles_layout.addWidget(widget, i, 0)  # Réajustement basé sur le nouvel index

            
    def remove_item(self):
        current_item = self.tree.currentItem()
        if current_item:
            item_id = current_item.data(0, Qt.UserRole).get("Index")
            if item_id in self.tile_widgets:
                widget_to_remove = self.tile_widgets[item_id]
                self.tiles_layout.removeWidget(widget_to_remove)
                widget_to_remove.setParent(None)
                widget_to_remove.deleteLater()
                del self.tile_widgets[item_id]  # Supprimer l'entrée du dictionnaire
            self.tree.takeTopLevelItem(self.tree.indexOfTopLevelItem(current_item))

class LayerTabWithFile(LayerTab):
    def __init__(self, tab_name="Layers"):
        super().__init__(tab_name)
        self.item_index = 0  # Initialiser un compteur pour l'index des items
        
        # Disconnect the old add_action connection if it exists
        try:
            self.add_action.triggered.disconnect()
        except TypeError:
            pass  # Handle the case where the signal was not connected before
        
        # Connect the new method to the action
        self.add_action.triggered.connect(self.open_file_dialog_and_add_item)

    def open_file_dialog_and_add_item(self):
        # Open file dialog to select a file
        file_path, _ = QFileDialog.getOpenFileName(self, "Open File", "", "Image Files (*.png *.jpg *.bmp);;All Files (*)")
        if file_path:
            # Extract file name without the extension
            file_name = file_path.split('/')[-1].rsplit('.', 1)[0]
            self.add_item_to_tree(file_name)

    def add_item_to_tree(self, item_name):
        # Create and add a new item to the tree with additional data
        new_item = QTreeWidgetItem([item_name])
        new_item.setData(0, Qt.UserRole, {
            "Visible": True,
            "Index": self.item_index
        })

        self.tree.addTopLevelItem(new_item)  # Assuming 'self.tree' is your QTreeWidget in LayerTab
        self.tree.setCurrentItem(new_item)

        self.item_index += 1  # Increment the index for the next item    
        
class LayerTabStandard(LayerTab):
    pass  # Pas de modification, utilise le comportement standard            

class SceneWidget(QWidget):
    def __init__(self):
        super().__init__()
        super().__init__()
        self.title = 'PyQt5 file dialogs - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480

        # Paramètres du widget
        self.setWindowTitle('QGraphicsScene Demo')
        #self.setGeometry(300, 300, 600, 400)
        self.setGeometry(self.left, self.top, self.width, self.height)
              
        self.initMenu()
        self.initTree()
        self.initTools()
                                    
        self.buildPack()
        self.initUI()

    def initMenu(self):
        # Configuration de la barre de menu
        self.menuView = QMenuBar()
        
        # Ajout de menus à la barre de menu
        fileMenu = self.menuView.addMenu('File')
        editMenu = self.menuView.addMenu('Edit')
        toolsMenu = self.menuView.addMenu('Tools')
        
        # Ajout d'actions au menu File
        openAction = QAction('Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.triggered.connect(self.openFileNameDialog)
        fileMenu.addAction(openAction)

        saveAction = QAction('Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.triggered.connect(self.openFileNameDialog)
        fileMenu.addAction(saveAction)

        exitAction = QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(self.openFileNameDialog)
        fileMenu.addAction(exitAction)
        
        # Ajout d'actions au menu Edit (exemples fictifs)
        cutAction = QAction('Cut', self)
        cutAction.setShortcut('Ctrl+X')
        editMenu.addAction(cutAction)

        copyAction = QAction('Copy', self)
        copyAction.setShortcut('Ctrl+C')
        editMenu.addAction(copyAction)

        pasteAction = QAction('Paste', self)
        pasteAction.setShortcut('Ctrl+V')
        editMenu.addAction(pasteAction)
        
        # Ajout d'actions au menu Tools (exemples fictifs)
        toolAction1 = QAction('Tool 1', self)
        toolsMenu.addAction(toolAction1)
        toolAction2 = QAction('Tool 2', self)
        toolsMenu.addAction(toolAction2)

    def initTree(self):

        # Création du QTreeView
        self.treeView = QTreeView()
        
        #self.model = QStandardItemModel()
        #self.rootNode = self.model.invisibleRootItem()
        
        self.model = QFileSystemModel()#QStandardItemModel()
        self.model.setRootPath('')
        self.treeView.setModel(self.model)
        # Définir le chemin du répertoire racine selon les besoins
        self.treeView.setRootIndex(self.model.index('.'))  

        self.treeView.setModel(self.model)
        self.treeView.expandAll()
               
    def initTools(self):

        # Création du QTabWidget
        self.toolBox = QTabWidget()
        
        self.toolBox.addTab(LayerTab("Layers"), "Layers")
        self.toolBox.addTab(LayerTabWithFileDialog("Tilesets"), "Tilesets")
        self.toolBox.addTab(LayerTabWithFile("Objects"), "Objects")
                  
    def buildPack(self):
      
        # Création de la scène
        self.scene = QGraphicsScene()

        # Layout vertical pour le QTreeView et le QToolBox
        self.treeLayout = QVBoxLayout()
        self.treeLayout.addWidget(self.treeView, 4)  # 40% de la hauteur pour le QTreeView
        self.treeLayout.addWidget(self.toolBox, 6)  # 60% de la hauteur pour le QToolBox
           
        # Création de la vue graphique
        self.view = QGraphicsView(self.scene, self)
                
        # Configuration du layout horizontal pour treeView et graphicsView
        hbox = QHBoxLayout()
        hbox.addLayout(self.treeLayout, 1)
        hbox.addWidget(self.view, 3)

        # Configuration du layout vertical principal
        vbox = QVBoxLayout()
        vbox.addWidget(self.menuView)
        vbox.addLayout(hbox)

        self.setLayout(vbox)
                
        self.show()
    
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            print(fileName)
        
        self.image = QImage(fileName)
        #pixmap = QPixmap.fromImage(self.image)
        #self.scene.addPixmap(pixmap)
        self.scene.addPixmap(QPixmap.fromImage(self.image))

         
        #pixmap = QPixmap(fileName)
        #pixmap_item = self.scene.addPixmap(pixmap)
        #pixmap_item.setPos(250, 70)
        #self.displayColorPalette(fileName)
        
        
    #def displayColorPalette(self, image_path):
        #image = QImage(fileName)
        #color_table = image.colorTable() if image.colorCount() > 0 else []
        #color_table = image.colorTable()  # récupère la table de couleurs
        #print( image.colorCount() )
        self.paletteWindow = ColorPaletteWindow(self.image, self.scene)
        self.paletteWindow.show()            

    def initUI(self):

        # Ajout d'éléments à la scène
        self.scene.addText("Bonjour PyQt5!")
        
        rect = QGraphicsRectItem(0, 0, 200, 50)

        # Set the origin (position) of the rectangle in the scene.
        rect.setPos(50, 20)

        # Define the brush (fill).
        brush = QBrush(Qt.red)
        rect.setBrush(brush)        
        self.scene.addItem(rect)
               
        #self.scene.addEllipse(QRectF(0, 0, 100, 100), QPen(QColor("blue")))
        

# Point d'entrée de l'application
def main():
    app = QApplication(sys.argv)
    ex = SceneWidget()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
