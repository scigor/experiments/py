from flask import Flask, request, jsonify
import os
import threading
import requests
import json
import civitai
from image_utils import *

import discord
from discord.ext import commands

app = Flask(__name__)

# Vérifie si le token a été récupéré
civitai_app_token = os.environ.get('CIVITAI_API_TOKEN')
if civitai_app_token is None:
    raise ValueError("La variable d'environnement 'CIVITAI_API_TOKEN' n'est pas définie.")

headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {civitai_app_token}"
}

# Creer un objet Intents avec uniquement les intents necessaires
intents = discord.Intents.default()
intents.message_content = True  # Activer uniquement l'intent pour lire les messages

# Initialiser le bot avec les intents
bot = commands.Bot(command_prefix="/", intents=intents)

# L'URL du webhook Discord où nous allons envoyer le message
DISCORD_WEBHOOK_URL = 'https://discord.com/api/webhooks/1295493286575739002/5c5JD_6Zn7fFYKsxdQOoCeYHIzlx7CFohAW7FLfddXPLiOn_BAh6VNULy6Fc5lWxZhdO'

def kobura():
    # input pour la création de l'image
    input = {
        "model": "urn:air:flux1:checkpoint:civitai:618692@691639",
        "params": {
            "prompt": "K0bura with blond hair styled in thick, sharp waves and pronounced sideburns. He is holding a cigar in his mouth while gazing upward with a confident and determined expression. He is wearing a brightly colored suit, featuring a yellow torso with a red emblem on the chest, and blue shoulder pads with black accents.The character’s muscular build and sharp facial features give him a heroic and bold appearance. capturing K0bura in a moment of heroic resolve.",
            "negativePrompt": "",
            "scheduler": "Euler",
            "steps": 25,
            "cfgScale": 3.5,
            "width": 512,
            "height": 512,
            "clipSkip": 2
        },
        "callbackUrl": "http://civitai.bteamcoders.212.47.250.95.sslip.io:5000/webhook",  # Point vers l'endpoint webhook
        "additionalNetworks": {
            "urn:air:flux1:lora:civitai:836239@935594": {
                "type": "Lora",
                "strength": 1.0,
            },
        },
    }
    # Appel à l'API Civitai (c'est un exemple, assure-toi que civitai.image.create est configuré correctement)
    response = civitai.image.create(input)

    # Retourne la réponse de Civitai
    return response

@bot.command(name='kobura')
async def discord_kobura(ctx):
    response = kobura()
    await ctx.send("Kobura a été appelé !") 
    # Retourne la réponse de Civitai
    print( "Response from kobura:", response )

@app.route('/kobura', methods=['GET'])
def flask_kobura():
    response =  kobura()
    # Retourne la réponse de Civitai
    return jsonify(response)

def send_to_discord(image_url):
    try:
        # Method1: Charger l'image en tant que fichier
        response = requests.get(image_url)

        image_thumbnail = thumbnail_io(response.content, (64, 64))
        image_resized = resize_io(response.content, (64, 64))
        image_pixeldetector, w, h, hf, vf = pixeldetector_io(response.content, palette=False, max_colors=128)

        files = {
            'file': ('image.png', response.content),
            'file1': ('image1.png', image_thumbnail),
            'file2': ('image2.png', image_resized),
            'file3': ('image3.png', image_pixeldetector)
        }
        embeds = [
            {
                "title": "Image1",
                "image": {
                    "url": "https://image.civitai.com/xG1nkqKTMzGDvpLrqFT7WA/0770e5fc-4260-4e3c-a0ee-bf2253028e9f/original=true,quality=90/x8_upscaled__00144_.jpeg"
                }
            }
        ]
        # Message formaté en Markdown
        message_content = (
            "```markdown\n"
           "Voici les différentes images:\n"
            "1. *Image d'origine* ![image.png](attachment://image.png)  \n"
            "2. **Image thumbtail(64x64)** ![image1.png](attachment://image1.png)  \n"
            "3. `Image resize(64x64)` ![image2.png](attachment://image2.png) \n"
            f"4. Image pixeldetector à {w}x{h}, Horizontal factor: {hf}, Vertical factor: {vf} ![image3.png](attachment://image3.png) \n"
            "> Chaque image a été traitée selon différentes étapes."
            "```"
        )
        payload = {
            "content": message_content
             #"embeds": embeds

        }
        # Envoyer l'image directement à Discord via un POST avec une pièce jointe
        discord_response = requests.post(DISCORD_WEBHOOK_URL, data=payload, files=files)
        #discord_response = requests.post(DISCORD_WEBHOOK_URL, json=payload)

        if discord_response.status_code == 204:
            return {"success": "Images envoyées avec succès"}, 200
        else:
            return {"error": "Erreur lors de l'envoi à Discord", "details": discord_response.text}, 500
    except Exception as e:
        print(f"Exception lors de l'envoi des images : {e}")
        return {"error": "Exception lors de l'envoi à Discord"}, 500

@app.route('/webhook', methods=['POST'])
def flask_webhook():
    # Récupère les données JSON envoyées par Civitai
    data = request.json
    print( "Received data:", data )

    if data["jobHasCompleted"] == True:
        job_id = data["jobId"]
        url = f'https://orchestration.civitai.com/v1/consumer/jobs/{job_id}'

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            data = response.json()
            print(json.dumps(data, indent=2))

            url = data["result"]["blobUrl"]
            print( url )

            return jsonify(send_to_discord(image_url))

    print()
    return jsonify({"error": "Aucune tâche à traiter"}), 400

@app.route('/post', methods=['GET'])
def flask_retry():

    image_url = 'https://image.civitai.com/xG1nkqKTMzGDvpLrqFT7WA/0770e5fc-4260-4e3c-a0ee-bf2253028e9f/original=true,quality=90/x8_upscaled__00144_.jpeg'

    return jsonify( send_to_discord( image_url ) )

# Fonction pour démarrer le bot Discord
def run_discord_bot():
    bot.run('MTI5NjE4NTc0ODc3MjYyMjQ3MQ.GFrUB-.ABDB7ToG0SQPpUuV1GwS5jrk4ubJ3s8r8IUifY')

# Fonction pour démarrer le serveur Flask
def run_flask_app():
    app.run('0.0.0.0',port=5000, debug=False)  # Lance le serveur Flask sur Port 5000

if __name__ == '__main__':
    # Lancer le bot Discord dans un thread séparé
    discord_thread = threading.Thread(target=run_discord_bot)
    discord_thread.start()

    # Lancer le serveur Flask dans un autre thread séparé
    flask_thread = threading.Thread(target=run_flask_app)
    flask_thread.start()

    # Attendre que les deux threads se terminent (facultatif)
    flask_thread.join()
    discord_thread.join()
