from flask import Flask, request, jsonify
import argparse
import os
import threading
import requests
import json
import civitai
#import asyncio

# Vérifie si le token a été récupéré
civitai_app_token = os.environ.get('CIVITAI_API_TOKEN')
if civitai_app_token is None:
    raise ValueError("La variable d'environnement 'CIVITAI_API_TOKEN' n'est pas définie.")

headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {civitai_app_token}"
}

# Configuration de la requête
query = {
    "properties": {
        "userId": 5611225
        #"username": 'b_teamcoders'
    }
}

detailed = False  # Optional boolean flag to get detailed job definitions. False by default.

async def fetch_jobs():
    try:
        response = await civitai.jobs.async_query(detailed=detailed, query=query)
        print( response )
        print( json.dumps(response, indent=2) )
    except Exception as e:
        print(f"Une erreur est survenue : {e}")

#help( civitai.jobs )
response = civitai.jobs.query( detailed, query )
#asyncio.run( fetch_jobs() )
