from flask import Flask, request, jsonify
import argparse
import os
import threading
import requests
import json
import civitai

# Vérifie si le token a été récupéré
civitai_app_token = os.environ.get('CIVITAI_API_TOKEN')
if civitai_app_token is None:
    raise ValueError("La variable d'environnement 'CIVITAI_API_TOKEN' n'est pas définie.")

headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {civitai_app_token}"
}

parser = argparse.ArgumentParser(
    description="Interroge un job sur civitai pour connaitre un status",
    usage="%(prog)s -t TOKEN -j JOB_ID"
)

arguments = [
    {
        'flags': ['-t', '--token'],
        'kwargs': {
            'type': str,
            'required': True,
            'help': "Token d'API pour l'authentification"
        }
    },
    {
        'flags': ['-j', '--job_id'],
        'kwargs': {
            'type': str,
            'required': True,
            'help': "ID du job à interroger"
        }
    }
]

# Ajout des arguments au parser
for arg in arguments:
    parser.add_argument(*arg['flags'], **arg['kwargs'])

# Analyse des arguments
args = parser.parse_args()

token = argparse.token
job_id = argparse.job_id

#help( civitai.jobs.get )
response = civitai.jobs.get(token=token,job_id=job_id)

print( response )
print( json.dumps(response, indent=2) )
