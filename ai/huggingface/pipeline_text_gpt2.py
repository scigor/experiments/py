from transformers import pipeline

model = "gpt2"

generator = pipeline("text-generation", model=model)
results = generator(
    "In this course, we will teach you how to",
    max_length=30,
    num_return_sequences=2
)

print(results)
