from transformers import AutoTokenizer, AutoModel

# Charger le modèle et le tokenizer
checkpoint = "distilbert-base-uncased"  # Utilisation d'un modèle général sans tête de classification
tokenizer = AutoTokenizer.from_pretrained(checkpoint)
model = AutoModel.from_pretrained(checkpoint)

# Préparer une entrée
input_text = "Transformers are amazing for NLP tasks!"
inputs = tokenizer(input_text, return_tensors="pt")

# Passer les entrées dans le modèle
outputs = model(**inputs)

# Obtenir les représentations contextuelles (embeddings)
embeddings = outputs.last_hidden_state  # Shape : (batch_size, sequence_length, hidden_size)

# Afficher les dimensions et un extrait des embeddings
print("Shape des embeddings :", embeddings.shape)

# Extrait des embeddings des 5 premiers tokens du batch, avec les 5 premières dimensions pour chaque token
print("Extrait des embeddings :", embeddings[0, :5, :5])  

