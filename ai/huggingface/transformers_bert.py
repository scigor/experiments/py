import torch
from transformers import AutoModel,AutoModelForSequenceClassification, AutoTokenizer, pipeline

checkpoint = "distilbert-base-uncased-finetuned-sst-2-english"
#model = "distilbert-base-multilingual-cased"
#model = "bert-base-uncased"
#checkpoint = model

# Charger le modèle et le tokenizer
#model = AutoModel.from_pretrained(checkpoint)
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

# Préparer le texte
raw_inputs = [
    "I've been waiting for a HuggingFace course my whole life.",
    "I hate this so much!",
]
inputs = tokenizer(raw_inputs, padding=True, truncation=True, return_tensors="pt")
print(inputs)
print(inputs["input_ids"])

outputs = model(**inputs)

if (hasattr(outputs, "last_hidden_state")):
 print(outputs.last_hidden_state.shape)

 print(outputs.logits.shape)
 print(outputs.logits)

predictions = torch.nn.functional.softmax(outputs.logits, dim=-1)
print(predictions)
