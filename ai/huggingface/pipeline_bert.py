from transformers import pipeline

#checkpoint = "distilbert-base-uncased-finetuned-sst-2-english"
#model = "distilbert-base-multilingual-cased"
model = "bert-base-uncased"

classifier = pipeline("sentiment-analysis", model=model)
results = classifier(
    [
        "I've been waiting for a HuggingFace course my whole life.",
        "I hate this so much!",
    ]
)

print(results)
