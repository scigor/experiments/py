from transformers import pipeline

model = "gpt2"

question_answerer = pipeline("question-answering")
results  = question_answerer(
    question="Where do I work?",
    context="My name is Sylvain and I work at Hugging Face in Brooklyn",
)

print(results)
