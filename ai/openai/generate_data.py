import csv
import random
from datetime import datetime, timedelta

# Configuration
num_hosts = 100       # Nombre d'hôtes uniques
num_days = 30           # Nombre de jours de données à générer
output_file = 'input.csv'

# Paramètres possibles
hosts = [f'host_{i}' for i in range(1, num_hosts + 1)]
hours = ['08:00', '10:00','12:00','14:00','16:00','18:00','20:00','22:00']

# Proportions des statuts par groupe d'hôtes
status_proportions = [
    {'S': 0.30, 'F': 0.23, 'E': 0.23, 'PS': 0.24},  # Pour les 2 premiers hôtes
    {'S': 0.50, 'F': 0.17, 'E': 0.17, 'PS': 0.16},  # Pour les 2 suivants
    {'S': 0.70, 'F': 0.10, 'E': 0.10, 'PS': 0.10},  # Pour les 2 suivants
    {'S': 0.50, 'F': 0.17, 'E': 0.17, 'PS': 0.16},  # Pour les 2 suivants
    {'S': 0.30, 'F': 0.23, 'E': 0.23, 'PS': 0.24}   # Pour les 2 derniers hôtes
]

def generate_random_date(start_date, num_days):
    """Génère une liste de dates sur une période de num_days jours à partir de start_date."""
    dates = [start_date + timedelta(days=i) for i in range(num_days)]
    return dates

def generate_status(proportions):
    """Génère un statut en fonction des proportions données."""
    statuses = list(proportions.keys())
    probabilities = list(proportions.values())
    return random.choices(statuses, probabilities, k=1)[0]

def generate_csv(file_name, hosts, hours, status_proportions, dates):
    """Génère le fichier CSV avec les contraintes spécifiées."""
    with open(file_name, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['nom_machine', 'timestamp', 'statut'])  # En-têtes du CSV

        for i, host in enumerate(hosts):
            group_index = i // 20  # Determine which group the host belongs to
            proportions = status_proportions[group_index]
            for date in dates:
                for hour in hours:
                    datetime_str = f"{date.strftime('%b %Y %d')}, {hour}:00"
                    statut = generate_status(proportions)
                    writer.writerow([host, datetime_str, statut])

if __name__ == "__main__":
    start_date = datetime.now() - timedelta(days=num_days)  # Date de début de la période
    dates = generate_random_date(start_date, num_days)
    generate_csv(output_file, hosts, hours, status_proportions, dates)

    print(f"Fichier CSV '{output_file}' généré avec succès.")
