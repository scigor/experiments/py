from flask import Flask, request, jsonify
import argparse
import os
import threading
import requests
import json
from openai import OpenAI

client = OpenAI()

def vision(url):
    response = client.chat.completions.create(
      model="gpt-4o-mini",
      messages=[
        {
          "role": "user",
          "content": [
            {"type": "text", "text": "What’s in this image?"},
            {
              "type": "image_url",
              "image_url": {
                "url": url,
              },
            },
          ],
        }
      ],
      max_tokens=300,
    )

    return response.choices[0]


def format_choice(choice):
    """Formate l'objet Choice pour une meilleure lisibilité."""
    formatted_result = {
        "index": choice.index,
        "finish_reason": choice.finish_reason,
        "message": {
            "role": choice.message.role,
            "content": choice.message.content
        }
    }
    return formatted_result


# Création du parseur avec une description personnalisée
parser = argparse.ArgumentParser(
    description="Extrait et enregistre une description d'1 image à partir d'une URL",
    usage="%(prog)s -o OUTPUT [--verbose] [URL]"
)

default_url='https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Gfp-wisconsin-madison-the-nature-boardwalk.jpg/2560px-Gfp-wisconsin-madison-the-nature-boardwalk.jpg'

arguments = [
    {
        'flags': ['-o', '--output'],
        'kwargs': {
            'type': str,
            #'required': True,
            'help': 'Nom du fichier de sortie'
        }
    },
    {
        'flags': ['-v', '--verbose'],
        'kwargs': {
            'action': 'store_true',
            'help': 'Afficher les informations détaillées'
        }
    },
    {
        'flags': ['url'],
            'kwargs': {
                'type': str,
                'nargs': '?',
                'default': default_url,
                'help': f"URL de l'image à télécharger (valeur par défaut : {default_url})"
            }
    }
]

# Ajout des arguments via une boucle
for arg in arguments:
    parser.add_argument(*arg['flags'], **arg['kwargs'])

# Analyse des arguments
args = parser.parse_args()

# Utilisation des arguments
filename = args.output
url = args.url
verbose = args.verbose

response = vision( url )
formatted_response = format_choice(response)
print( json.dumps(formatted_response, indent=2) ) 
