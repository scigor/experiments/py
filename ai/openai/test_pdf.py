#import pytesseract

# Spécifie le chemin d'installation de Tesseract
#pytesseract.pytesseract.tesseract_cmd = "/usr/bin/tesseract"  # Remplace par le chemin correct si nécessaire

# Test
#print(pytesseract.get_tesseract_version())

import os

#os.environ["OCR_AGENT"] = "unstructured_pytesseract.PytesseractAgent"
os.environ["OCR_AGENT"] = ""


from langchain_community.document_loaders import OnlinePDFLoader
from langchain_community.document_loaders import UnstructuredPDFLoader

#local_path = '/home/scigor/Bureau/Dev/py/ai/openai/Neo-Geo-JAH.pdf'
local_path = '/home/scigor/Bureau/Dev/py/ai/openai/WEF_The_Global_Cooperation_Barometer_2024.pdf'

if local_path:
    loader = UnstructuredPDFLoader(local_path)
    loader.partitioning_strategy = "hi_res"  
    data = loader.load()
else:
    print('No path provided')

print(data[0].page_content)
#print(data.page_content)




