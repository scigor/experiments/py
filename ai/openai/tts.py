from pathlib import Path
from openai import OpenAI

client = OpenAI()

response = client.audio.speech.create(
  model="tts-1",
  voice="alloy",
  input="Today is a wonderful day to build something people love!"
)

speech_file_path = Path(__file__).parent / "speech.mp3"
response.stream_to_file( speech_file_path )
