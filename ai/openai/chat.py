from openai import OpenAI

client = OpenAI()

completion = client.chat.completions.create(
    model="gpt-4o-mini",
    messages=[
        {"role": "system", "content": "You are a helpful assistant."},
        {
            "role": "user",
            "content": "Write a sample flask python server."
        }
    ]
)

# Récupérer le contenu de la réponse
response_content = completion.choices[0].message.content

# Afficher le résultat de manière structurée
print( "Voici un exemple de serveur Flask généré par l'API OpenAI :\n" )
print( response_content )
