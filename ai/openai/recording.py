import sounddevice as sd
from scipy.io.wavfile import write

# Paramètres d'enregistrement
samplerate = 44100  # Taux d'échantillonnage (Hz)
duration = 10       # Durée de l'enregistrement (en secondes)
filename = "voice.wav"

print("Enregistrement en cours...")
# Capturer l'audio
audio_data = sd.rec(int(samplerate * duration), samplerate=samplerate, channels=1, dtype='int16')
sd.wait()  # Attendre la fin de l'enregistrement
print("Enregistrement terminé.")

# Sauvegarder au format WAV
write(filename, samplerate, audio_data)
print(f"Audio enregistré dans le fichier : {filename}")
