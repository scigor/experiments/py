import pytesseract
from PIL import Image

# Spécifie où se trouve Tesseract sur ton système (si nécessaire)
pytesseract.pytesseract.tesseract_cmd = "/usr/bin/tesseract"  # ou le chemin correct pour ton système

# Ouvrir une image de test
img = Image.open('/home/scigor/Bureau/Dev/py/ai/openai/images.png')  # Remplace par le chemin de ton image

# Extraire le texte avec pytesseract
text = pytesseract.image_to_string(img)

# Afficher le texte extrait
print("Texte extrait :", text)
