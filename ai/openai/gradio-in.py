from dataclasses import dataclass, field

from langchain_core.tools import Tool

from langchain_openai import ChatOpenAI
from langchain.schema import AIMessage, HumanMessage, SystemMessage

from transformers import pipeline
import soundfile as sf
import pyttsx3

import openai
from openai import OpenAI

import gradio as gr
import numpy as np
import io
import os

stt_pipeline = pipeline("automatic-speech-recognition", model="openai/whisper-small")

tts_pipeline = pipeline("text-to-speech", model="facebook/mms-tts-fra")

def transcribe_audio(file_path):
    return stt_pipeline(file_path)["text"]

whisper_tool = Tool(
    name="Speech-to-Text",
    func=transcribe_audio,
    description="Convertit un fichier audio en texte avec Whisper"
)

def text_to_speech(text):
    '''
    tts = pyttsx3.init()
    tts.say(text)
    tts.runAndWait()
    '''
    tts_pipeline(text)

tts_tool = Tool(
    name="Text-to-Speech",
    func=text_to_speech,
    description="Lit un texte à haute voix avec un moteur TTS"
)

import tempfile
from pydub import AudioSegment

import base64
from pathlib import Path

os.environ["OPENAI_API_KEY"] = "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" 
client = OpenAI()

#llm = ChatOpenAI(temperature=1.0, model='gpt-4o')
llm = ChatOpenAI(temperature=1.0, base_url="http://localhost:11434/v1", model='llama3', api_key="your_api_key")

OUT_RATE = 24000
OUT_SAMPLE_WIDTH = 2
OUT_CHANNELS = 1

@dataclass
class AppState:
    def __init__(self, conversation=None, transcription=None, conversation_json=None, audio=None):
        self.conversation = conversation if conversation is not None else []
        self.transcription = transcription if transcription is not None else []
        self.conversation_json = conversation_json if conversation_json is not None else ""
        self.audio = audio if audio is not None else ""
    stream: np.ndarray | None = None
    sampling_rate: int = 0
    pause_detected: bool = False
    started_talking: bool = False
    stopped: bool = False
    #conversation: list = field(default_factory=list) # ??

def process_audio(audio: tuple, state: AppState):
    print("Début de la fonction process_audio")
    print(f"Audio reçu : {audio[1].shape}, Fréquence : {audio[0]}")
    if state.stream is None:
        print("Initialisation du flux audio")
        state.stream = audio[1]
        state.sampling_rate = audio[0]
    else:
        print("Ajout au flux audio existant")
        state.stream = np.concatenate((state.stream, audio[1]))

    #pause_detected = determine_pause(state.stream, state.sampling_rate, state)
    #state.pause_detected = pause_detected

    #if state.pause_detected and state.started_talking:
    if state.started_talking:
        print("Arrêt de l'enregistrement détecté")
        return gr.Audio(recording=False), state
    return None, state

def reset_audio(state: AppState):
    if not state.stopped:
        return gr.Audio(recording=True)

def speaking(audio_bytes: str):

    # Encoder en Base64
    base64_encoded = str(base64.b64encode(audio_bytes), encoding="utf-8")
    #base64_encoded = base64.b64encode(audio_bytes).decode("utf-8")

    completion = client.chat.completions.create(
        model="gpt-4o-audio-preview",
        modalities=["text", "audio"],
        audio={"voice": "alloy", "format": "wav"},
        messages=[
            {
                "role": "user",
                "content": [
                    { 
                        "type": "text",
                        "text": "answer in french?"
                    },
                    {
                        "type": "input_audio",
                        "input_audio": {
                            "data": base64_encoded,
                            "format": "wav"
                        }
                    }
                ]
            },
        ]
    )
    #print(completion.choices[0])

    wav_bytes = base64.b64decode(completion.choices[0].message.audio.data)
    with open("recording.wav", "wb") as f:
      f.write(wav_bytes)
    '''
    audio_segment = AudioSegment(
        wav_bytes,
        frame_rate=OUT_RATE,
        sample_width=OUT_SAMPLE_WIDTH,
        channels=OUT_CHANNELS,
    )
    '''
    # Chargement du fichier WAV
    wav_io = io.BytesIO(wav_bytes)
    audio_segment = AudioSegment.from_wav(wav_io)

    # Vérifier si l'audio est valide avant d'exporter
    if audio_segment.duration_seconds > 0:
        print("Audio chargé avec succès")
    else:
        raise ValueError("L'audio est vide ou invalide.")
        
    # Conversion en MP3
    mp3_io = io.BytesIO()
    audio_segment.export(mp3_io, format="wav")# bitrate="320k")
    mp3_bytes = mp3_io.getvalue()
    mp3_io.close()

    return mp3_bytes

def chatbot_handler_audio(state: AppState):
    # Format Langchain
    history_langchain_format = [SystemMessage(content="Tu es un assistant vocal.")] 

    #if not state.pause_detected and not state.started_talking:
    #if not state.started_talking:
    #    print("Aucun enregistrement actif détecté")
    #    return None, AppState()

    if state.stream is None:
        print("Erreur : aucun flux audio actif.")
        return None, AppState()

    # Traitement de l'audio et transcription
    input_audio_buffer = io.BytesIO()

    input_audio_segment = AudioSegment(
        state.stream.tobytes(),
        frame_rate=state.sampling_rate,
        sample_width=state.stream.dtype.itemsize,
        channels=(1 if len(state.stream.shape) == 1 else state.stream.shape[1]),
    )
    input_audio_segment.export(input_audio_buffer, format="wav")

    # Création du fichier temporaire pour l'audio
    with tempfile.NamedTemporaryFile(suffix=".wav", delete=False) as f:
        f.write(input_audio_buffer.getvalue())

    state.conversation.append(
        {"role": "user", "content": {"path": f.name, "mime_type": "audio/wav"}})

    input_audio_file = open(f.name, "rb")
    transcription = client.audio.transcriptions.create(
        model="whisper-1", 
        file=input_audio_file
    )
    # Enregistrer la transcription dans l'état
    state.transcription.append(
        {"role": "user", "content": {transcription.text}})

    # Créer un buffer audio de sortie (audio généré par le bot)
    output_audio_buffer = speaking(input_audio_buffer.getvalue())

    # Sauvegarde de l'audio généré
    with tempfile.NamedTemporaryFile(suffix=".wav", delete=False) as f:
        f.write(output_audio_buffer)

    #Enregistrer le Chemin audio dans l'état
    state.audio = f.name

    state.conversation.append(
        {"role": "assistant", "content": {"path": f.name, "mime_type": "audio/wav"}})
    
    output_audio_file = open(f.name, "rb")
    transcription = client.audio.transcriptions.create(
        model="whisper-1", 
        file=output_audio_file 
    )

    # Mise à jour de la conversation avec l'audio et la transcription
    state.transcription.append(
        {"role": "assistant", "content": {transcription.text}})

    # Retourner la transcription et l'audio pour Gradio
    yield None, AppState(conversation=state.conversation, transcription=state.transcription)


def chatbot_handler_text(message, state: AppState):
    # Format Langchain
    history_langchain_format = [SystemMessage(content="Tu es un assistant vocal.")] 

    # Format Gradio - Init a vide si None
    state.conversation = state.conversation or []      

    # Convertir l'historique conversation existant en format LangChain
    for msg in state.conversation:
        if msg["role"] == "user":
            history_langchain_format.append(HumanMessage(content=msg["content"]))
        elif msg["role"] == "assistant":
            history_langchain_format.append(AIMessage(content=msg["content"]))

    # Ajoutez le message utilisateur    
    history_langchain_format.append(HumanMessage(content=message))

    gpt_response = llm.invoke(history_langchain_format)
    
    # Ajouter la réponse de l'assistant
    history_langchain_format.append(AIMessage(content=gpt_response.content))

    # Ajoutez le message utilisateur et la réponse de l'assistant
    state.conversation.append({"role": "user", "content": message})
    state.conversation.append({"role": "assistant", "content": gpt_response.content})
        
    #tts_tool.run(gpt_response.content)
    output = tts_pipeline(gpt_response.content)
    #print(output)
    
    audio_data = output["audio"].flatten()
    sampling_rate = output["sampling_rate"]

    # Créer un buffer en mémoire
    output_audio_buffer = io.BytesIO()
    sf.write(output_audio_buffer, audio_data, sampling_rate, format="WAV")
    output_audio_buffer.seek(0)  # Revenir au début du buffer

    # Sauvegarde de l'audio généré
    with tempfile.NamedTemporaryFile(suffix=".wav", delete=False) as f:
        f.write(output_audio_buffer.read())
    
    print(f"🔊 Audio généré : {f.name}")
    state.audio = f.name        

    '''    
    state.audio = text_to_speech("texte de test")
    '''        
    
    # Retournez les données pour les composants Gradio
    return None, AppState(conversation=state.conversation), state.conversation, state.audio

def chatbot_handler_dummy(message, state: AppState):
    # Format Gradio - Init a vide si None
    state.conversation = state.conversation or []  

    state.conversation.append({"role": "user", "content": message})
    state.conversation.append({"role": "assistant", "content": f"Vous avez dit : {message}"})

    # Retournez les données pour les composants Gradio
    # raz input_text, new AppState et conversation historique
    return None, AppState(conversation=state.conversation), state.conversation 


def text_to_speech(text):
    # Fonction TTS simulée (remplace-la par le tts_pipeline)
    audio_data = np.random.randn(16000)  # Remplace cela par la sortie de ton modèle TTS
    sampling_rate = 16000  # Remplace cela par la valeur correcte du modèle TTS

    # Sauvegarder l'audio généré dans un fichier temporaire
    file_path = tempfile.mktemp(suffix=".wav")  # Créer un fichier temporaire
    with open(file_path, "wb") as f:
        sf.write(f, audio_data, sampling_rate, format="WAV")

    return file_path  # Renvoyer le chemin du fichier audio

with gr.Blocks() as demo:
  with gr.Row():
    with gr.Column():      
      input_text = gr.Textbox(label="Input Text")
      input_audio = gr.Audio(label="Input Audio", sources="microphone", type="numpy")
      submit = gr.Button("Envoyer")
      #im = gr.ImageEditor()
    with gr.Column():
      chatbot = gr.Chatbot(label="Conversation", type="messages")
      aaudio = text_to_speech("texte de test")
      output_audio = gr.Audio(label="Output Audio", streaming=True, autoplay=True)            
      chatbotjson = gr.JSON(label="Historique brut (JSON)")

  state = gr.State(value=AppState())

  stream = input_audio.stream(process_audio,
      [input_audio, state],
      [input_audio, state],
      stream_every=0.5,
      time_limit=30,
  )  

  respond_audio = input_audio.stop_recording(chatbot_handler_audio,
      [state],
      [output_audio]
  )
  # Mettre à jour la conversation dans le chatbot
  respond_audio.then(lambda s: s.conversation, [state], [chatbot])

  # Mettre à jour l'audio dans output_audio
  respond_audio.then(lambda s: s.audio, [state], [output_audio])

  # Mettre à jour la transcription dans chatbotjson
  respond_audio.then(lambda s: s.transcription, [state], [chatbotjson])

  restart = output_audio.stop(reset_audio,
      [state],
      [input_audio]
  )
  
  # Soumission du texte
  '''
  respond_submit = submit.click(fn=chatbot_handler_text,
               inputs=[input_text, state], 
               outputs=[input_text, state, chatbotjson, output_audio])
  '''
  respond_submit = submit.click(fn=chatbot_handler_text,
               inputs=[input_text, state], 
               outputs=[output_audio])

  respond_submit.then(lambda s: s.conversation, [state], [chatbot])
  respond_submit.then(lambda s: s.audio, [state], [output_audio])

if __name__ == "__main__":
    demo.launch()
    #chat = gr.ChatInterface(predict, type="messages").launch()
