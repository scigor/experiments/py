import requests
from openai import OpenAI

client = OpenAI()

def save(image_url,image_name):
    # Télécharger l'image
    image_response = requests.get(image_url)

    # Vérifier si le téléchargement a réussi
    if image_response.status_code == 200:
        # Sauvegarder l'image dans un fichier
        with open(image_name, "wb") as file:
            file.write(image_response.content)
        print("L'image a été sauvegardée.'")
    else:
        print(f"Erreur lors du téléchargement de l'image : {image_response.status_code}")


#if __name__ == "__main__":
response = client.images.generate(
  model="dall-e-3",
  prompt="two wooden stilt cabins on the beach, retro aesthetic with their reflections in the shallow puddles left by the tide, No floors on the cabin on stilts",
  size="1024x1024",
  quality="standard",
  n=1,
)

image_url = response.data[0].url
save( image_url, 'cabin.png' )
