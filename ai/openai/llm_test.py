from langchain.chains import LLMChain
from langchain_openai import ChatOpenAI
from langchain_core.prompts import ChatPromptTemplate
from langchain_core.prompts import PromptTemplate
from langchain_core.output_parsers import StrOutputParser
from langchain.schema import AIMessage, HumanMessage

llm = ChatOpenAI(base_url="http://localhost:11434/v1", model='llama3', api_key="your_api_key")

"""
prompt = ChatPromptTemplate([
    ("system", "Hello, how can I help you today?"),
    ("user", "I would like to know the meaning of life."),

])
"""

prompt = PromptTemplate(
	input_variables=["question"],
	template="Question: {question}\nRéponse:")

conversation_history = []

chain = prompt | llm | StrOutputParser()

# Debug Par exemple
#help(chain.stream)
#help('langchain.schema.Input') 

def conversation_with_stream(question):
    # Variable pour récupérer la réponse complète
    response_text = ""

    # Streaming des réponses et ajout de l'historique au fur et à mesure
    for response in chain.stream({"question": question}):  # Utilisation de stream directement
        response_text += response
        print(response, end="", flush=True)  # Affichage du streaming

    return response_text

# Fonction avec historique utilisant invoke
def conversation_with_history(question):
    # Ajouter la question de l'utilisateur à l'historique
    conversation_history.append({"role": "user", "content": question})

    # Convertir l'historique de la conversation en format Langchain
    history_langchain_format = [HumanMessage(content=msg["content"]) if msg["role"] == "user" 
                                else AIMessage(content=msg["content"]) 
                                for msg in conversation_history]

    # Passer l'historique complet au modèle à chaque itération
    gpt_response = llm.invoke(history_langchain_format)  # Passer l'historique mis à jour
    
    # Afficher la réponse
    print(gpt_response.content)

    # Ajouter la réponse du modèle à l'historique pour la prochaine itération
    conversation_history.append({"role": "assistant", "content": gpt_response.content})
    history_langchain_format.append(AIMessage(content=gpt_response.content))

    return gpt_response.content

# Exemple d'utilisation
response1 = conversation_with_stream("Quel est la capitale de la France ?")
print
response2 = conversation_with_stream("Et celle de la Pologne ?")
