from PIL import Image

import io
import sys
import time

original_argv = sys.argv
sys.argv = ['pixeldetector.py', '-i', 'fake_input.png']
from pixeldetector import pixel_detect, determine_best_k
sys.argv = original_argv

def pixeldetector_io(image_data, palette=False, max_colors=128):
    try:
        # Ouvrir l'image à partir des données en bytes
        image = Image.open(io.BytesIO(image_data)).convert('RGB')

        # Appeler la fonction pixel_detect depuis pixeldetector.py
        downscale_image, hf, vf = pixel_detect(image)
        
        print(f"Image réduite à {downscale_image.width}x{downscale_image.height}, Horizontal factor: {hf}, Vertical factor: {vf}")

        # Choisir la plus grande échelle entre hf et vf
        scale = max(hf, vf)
        output = downscale_image

        # Si l'option palette est activée, réduire la palette de couleurs
        if palette:
            # Mesurer le temps de démarrage
            start = round(time.time() * 1000)

            # Réduire la palette de couleurs en utilisant la méthode du coude
            best_k = determine_best_k(downscale_image, max_colors)
            output = downscale_image.quantize(colors=best_k, method=1, kmeans=best_k, dither=0).convert('RGB')

            print(f"Palette réduite à {best_k} couleurs en {round(time.time() * 1000) - start} millisecondes")

        # Sauvegarder l'image traitée dans un buffer en mémoire
        img_byte_arr = io.BytesIO()
        output.save(img_byte_arr, format='PNG')
        img_byte_arr.seek(0)  # Remettre le pointeur au début du buffer

        return img_byte_arr.getvalue(), downscale_image.width, downscale_image.height, hf, vf

    except Exception as e:
        print(f"Erreur lors du traitement de l'image : {e}")
        return None

def thumbnail_io(image_data, max_resolution):
    try:
        # Ouvrir l'image
        img = Image.open(io.BytesIO(image_data))
        
        # Créer une miniature
        img.thumbnail(max_resolution)
        
        # Sauvegarder l'image dans un buffer en mémoire
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')
        img_byte_arr.seek(0)  # Remettre le pointeur au début du buffer
        
        return img_byte_arr.getvalue()
    except Exception as e:
        print(f"Erreur lors de la création du thumbnail : {e}")
        return None

def resize_io(image_data, resolution):
    try:
        # Ouvrir l'image
        img = Image.open(io.BytesIO(image_data))
        
        # Redimensionner l'image
        resized_img = img.resize(resolution)
        
        # Sauvegarder l'image dans un buffer en mémoire
        img_byte_arr = io.BytesIO()
        resized_img.save(img_byte_arr, format='PNG')
        img_byte_arr.seek(0)  # Remettre le pointeur au début du buffer
        
        return img_byte_arr.getvalue()
    except Exception as e:
        print(f"Erreur lors du redimensionnement de l'image : {e}")
        return None

# Exemple de test
if __name__ == "__main__":
    with open("cabin.png", "rb") as f:
        image_data = f.read()

    thumbnail = thumbnail_io(image_data, (64, 64))
    resized = resize_io(image_data, (128, 128))

    with open("thumbnail.png", "wb") as f:
        f.write(thumbnail)

    with open("resized.png", "wb") as f:
        f.write(resized)

    print("Images créées avec succès!")
    pixeldetection = pixeldetector_io(image_data, palette=False, max_colors=256)
    with open("pixeldetector.png", "wb") as f:
        f.write(pixeldetection)

