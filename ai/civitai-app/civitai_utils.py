from flask import Flask, Blueprint, render_template, request, jsonify
from flask_wtf import FlaskForm
from wtforms import SelectField, TextAreaField, BooleanField, SubmitField
from wtforms.fields import StringField
from wtforms.validators import DataRequired
import re

# Charger les données
def load_data():
    models = []
    loras = []
    with open("models.txt", "r") as file:
        for line in file:
            if "checkpoint" in line:
                models.append(line.strip())
            elif "lora" in line:
                loras.append(line.strip())
    return models, loras

models_data, loras_data = load_data()

# Fonction pour filtrer les modèles et loras
def filter_loras(selected_model):
    print(f"Selected Model: {selected_model}")  # Vérifie ce que contient selected_model

    model_type_match = re.search(r"sdxl|sd1|flux", selected_model)
    if not model_type_match:
        print("No valid model type found.")
        return []

    model_type = model_type_match.group()
    print(f"Model Type: {model_type}")  # Vérifie le type de modèle extrait

    filtered_loras = [lora for lora in loras_data if model_type in lora]
    print(f"Filtered Loras: {filtered_loras}")  # Affiche les Loras filtrés
    return filtered_loras


class CivitaiForm(FlaskForm):
    model = SelectField("Model", choices=[], validators=[DataRequired()])
    lora = SelectField("Lora", choices=[("n/a", "N/A")])
    user_pos_input = TextAreaField("Positive Input", validators=[DataRequired()])
    user_neg_input = TextAreaField("Negative Input")
    output = StringField("Output", render_kw={"readonly": True, "placeholder": "Output will appear here..."})
    
    # Checkboxes pour les options
    simple_background = BooleanField("Simple Background")
    full_body = BooleanField("Full Body")
    
    submit = SubmitField("Post")

civitai_blueprint = Blueprint('civitai', __name__)

# Route principale
@civitai_blueprint.route('/civitai', methods=['GET', 'POST'])
def civitai():
    selected_model = request.json.get('model')
    filtered_loras = filter_loras(selected_model)
    return jsonify({
        "loras": filtered_loras
    })
"""
    form = CivitaiForm()
    form.model.choices = [(model, model) for model in models_data]

    # Gérer la sélection du modèle et filtre des Loras
    if request.method == 'POST':
        selected_model = request.form.get('model')
        filtered_loras = filter_loras(selected_model)
        form.lora.choices = [(lora, lora) for lora in filtered_loras] or [("n/a", "N/A")]

        # Gérer la sortie "output"
        pos_input = request.form.get('user_pos_input', '')
        selected_labels = [label for label in ["simple_background", "full_body"] if request.form.get(label)]
        form.output.data = f"{pos_input} {' '.join(selected_labels)}"

        if form.validate_on_submit():
            # Logic to process the form submission
            return jsonify({"status": "Form submitted"})

    # Si c'est un GET, initialiser les champs avec les valeurs par défaut
    return render_template("civitai.html", form=form)
"""
